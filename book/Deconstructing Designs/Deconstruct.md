## Deconstruct

Though there are always variations in rectangle count for a given deconstructed solution, ours will be composed of twenty-seven. Was your answer closer to eleven? If so, there is one fundamental area of improvement for you to focus on. Remember, we are designing for *dynamic* not *static* layouts. Tactically this translates to thinking more in:
- Percentages and ratios vs. exact pixel dimensions
- Content anchored in nested containers vs. absolutely positioned within a single container

> Thinking and designing for dynamic—not static—layouts is the single biggest step to leveling up as a designer.

Twenty-seven may seem like a high number for such a simple UI, but when thinking and designing for dynamic layouts this isn't surprising. How exactly did we come to this twenty-seven number though? There are two core approaches for tracing out the rectangles:
1. Mentally
1. In your design tool of choice

You will improve with the former approach in time. If you consistently nest and organize the layers in your design tool of choice, you are well set for the second approach. In either case, the deconstruction process has three core substeps:

1. Rectangle - *Determine the rectangle count*
1. Nest - *Determine the parent-child relationships*
1. Tree - *Create a tree structure using these facts*

Each nesting relates to the parent-child relationship we talked about earlier in the
[*Structure*](/structure-style-and-behavior/#structure) section. Below is the result of our three step deconstruction process using an HTML-like code. Each tree node is composed of:
- An implied rectangle
- A proposed HTML element
- Contextual information

```html
<div> main
    <div> avatar
        <img> avatar graphic
    <div> actions
        <div> top
            <p> What's happening? text
        <div> bottom
            <div> left
                <button> photo
                    <img> photo graphic
                <button> gif
                    <img> gif graphic
                <button> poll
                    <img> poll graphic
                <button> emoji
                    <img> emoji graphic
                <button> more...
                    <img> more... graphic
            <div> right
                <div> progress bar
                    <img> progress bar graphic
                <div> divider
                    <img> divider graphic
                <button> add
                    <img> plus graphic
                <button> Tweet
                    <span> Tweet text
```
