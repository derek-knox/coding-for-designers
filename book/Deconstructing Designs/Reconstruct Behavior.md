## Reconstruct Behavior

This process consists of three core substeps:
1. Change - *Determine what can change at execution time*
1. Depend - *Determine the change dependencies*
1. Component - *Create components using these facts*
    - Shell - *Code the component shell*
    - Interface - *Code the component interface*
    - Implementation - *Code the component implementation*

### Change

For this substep the goal is to define what can *change* at execution time. This is determined from general experience, specific product use, or through mental simulation. The latter case is especially true when designing something new from scratch. In either case, the goal is to list what will change at execution time based on the aforementioned *input triggers*. As a reminder these triggers are:

1. User interaction (tap, click, hover, gesture, voice, etc.)
2. Environment (layout resizing, operating system, device sensors, etc.)
3. Time (delays, schedules, etc.)

For the Tweet UI, our change list includes the following:

1. What's happening? focused state & visibility
1. Tweet text
1. Progress bar Tweet character limit
1. Enabled vs. Disabled state of Tweet button
1. Tweet media button popups
1. Add Tweet button popup

Moving forward we'll ignore the latter two popup related changes to scope down this chapter's length. We will focus solely on the Tweet message text and its impact on the rest of the UI. This subset focus will suffice as it encompasses all four combinations of state changes that can occur in interactive applications. These are:
1. Binary & Source
1. Binary & Derived
1. Many & Source
1. Many & Derived

You can likely infer the combined meaning of each, but we'll detail them below soon.

Now that we've defined what *can change* at execution time we may begin to define the *dependencies* of those changes.

### Depend

Inherent in change is the implied fact that there are *states*. [Programming and Visual Design - Texture and State](/programming-and-visual-design/#texture-and-state) first introduced us to states in the context of coding:

> Texture when applied to a shape or a form gives it a richer quality. This richness is exemplified as a sense of time, where a texture is aged and weathered for example. Similarly, a function or object with state gives it a richer quality, a sense of time as well.

If something can change, it is at the very least in *this* state or *that* state at any moment in time. What does this remind you of?

Yup, binary. Defining and intuiting what is binary or not takes time to get used to, but it is a straightforward concept to apply in UIs. In fact, there are two questions to ask after determining the change list:

- What has *only two states*? (Binary)
- What has *three or more states*? (Many)

We can ask ourselves these questions for each item in our change list to get an answer. Specifically for our Tweet UI change list we get:

1. What's happening? focused state (Binary) & visibility (Binary)
    - Focused
    - Unfocused
    - Visible
    - Invisible
1. Tweet text (Many)
    - Zero characters
    - One character
    - Two characters
    - Etc.
1. Progress bar Tweet character limit (Many)
    - Zero percent
    - One percent
    - Two percent
    - Etc.
1. Enabled vs. Disabled state of Tweet button (Binary)
    - Enabled
    - Disabled

Using the below refresher from [80/20 JavaScript - Types & Forms](80-20-javascript/#types-forms) section, try to determine the best-match *value type* to represent the state of each item in the change list.

1. Primitive Values
    - null (`null`)
    - undefined (`undefined`)
    - Boolean (`true` & `false`)
    - Number (`360`)
    - String (`"one or more characters wrapped in double quotes"` or `'single quotes'`)
2. Complex Values
    - Object (`{}` & `[]`)

Using this information, what is the best-match value type for each item in our change list? The result we're looking for is:

1. What's happening? focused state (`Boolean`/Binary) & visibility (`Boolean`/Binary)
1. Tweet text (`String`/Many)
1. Progress bar Tweet character limit (`Number`/Many)
1. Enabled vs. Disabled state of Tweet button (`Boolean`/Binary)

After determining the value type of each item in the change list, we can proceed more informed. In fact we can further help define the change dependencies through identifying the change list item's *state type*. There are only two:
1. Source (explicit state)
1. Derived (implied state)

We have not explored the two *state types* as they were not important until now. They will help us implement our behavior code with less duplication. Remember [Don't Repeat Yourself](80-20-javascript/#4-don-t-repeat-yourself)? This is where *derived* state shines.

Each primitive or complex value in an application is either source or derived. That fact depends solely on the expected behavior of the application in question. In the context of the Tweet Box UI that means we need to determine which (if any) of our change list item's have their state set explicitly (source) or should be implied (derived). Thinking in terms of derived state takes time to get used to, but the better you get at it the less redundant your code can become. Thankfully there is one simple question to ask to help determine if state is source or derived:

> Can the state of something be defined in terms of the state of something else?

Before reading on, ask the above question against each of the four change list items and their defined value types. Which values can be derived? This will be challenging as it takes general experience, specific product use, and mental simulation, but try your best. Here is what we're looking for:

1. What's happening? focused state (`Boolean`/Binary & Source) & visibility (`Boolean`/Binary & Derived)
1. Tweet text (`String`/Many & Source)
1. Progress bar Tweet character limit (`Number`/Many & Derived)
1. Enabled vs. Disabled state of Tweet button (`Boolean`/Binary & Derived)

Ask yourself, what makes the latter two derived? Using our helper question for each, what is the *something else* that would make it so? Feel free to think through this before moving on as we reveal the answers below.

To complete this substep and empower us in the [Component](#component) substep we must define our change dependencies by noting their input triggers and source vs. derived state type. Here we go:

1. What's happening? focused state (`Boolean`/Binary & Source) & visibility (`Boolean`/Binary & Derived)
    - Focused
        - User interaction (click/tap in)
        - User interaction (tab keyboard in)
    - Unfocused
        - User interaction (click/tap out)
        - User interaction (tab keyboard out)
    - Visible
        - Derived (from Tweet text `String` length)
    - Invisible
        - Derived (from Tweet text `String` length)
2. Tweet text (`String`/Many & Source)
    - Zero characters
        - User interaction (key down/up)
        - User interaction (cut/paste)
    - One character
        - User interaction (key down/up)
        - User interaction (cut/paste)
    - Two characters
        - User interaction (key down/up)
        - User interaction (cut/paste)
    - Etc.
        - User interaction (key down/up)
        - User interaction (cut/paste)
3. Progress bar Tweet character limit (`Number`/Many & Derived)
    - Zero percent
        - Derived (from Tweet text `String` length)
    - One percent
        - Derived (from Tweet text `String` length)
    - Two percent
        - Derived (from Tweet text `String` length)
    - Etc.
        - Derived (from Tweet text `String` length)
4. Enabled vs. Disabled state of Tweet button (`Boolean`/Binary & Derived)
    - Enabled
        - Derived (from Tweet text `String` length)
    - Disabled
        - Derived (from Tweet text `String` length)

Now we have what we need to inform how we structure our custom *components*. Let's to this.

### Component

We first described *components* in the [Programming & Visual Design - Constructs and Components](/programming-and-visual-design/#constructs-and-components) section:

> In programming, components embody the elements and design patterns in an effort to encapsulate specific functionality and ways of communicating with other components.

Additionally in [80/20 JavaScript - New Operator](80-20-javascript/#new-operator), we learned that objects are very useful containers for such a thing:

> ... use the `new` operator and think of it as your helper for getting unique instances of built-in, third-party, and custom types of Objects

Since we are creating non-built-in and non-third-party components, they must each be custom. Since there are four change list items, you might think we need four components. Sometimes the relationship is one-to-one like that, but sometimes it is not. In this particular case we can merge the first two into one component.

1. `TweetText` (What's happening? and Tweet text)
1. `ProgressBar`
1. `TweetButton`

By following the same useful pattern that was first demonstrated in the `Artboard` example. We can code the *shell*.

#### Shell

Defining a shell for each component is extremely simple and consists of the following:

1. Define a `function` and name it
2. Define its `api`
3. `return` its `api`

Our three components become:

```javascript
function TweetText() {
    var api = {};
    return api;
}

function ProgressBar() {
    var api = {};
    return api;
}

function TweetButton() {
    var api = {};
    return api;
}
```

Admittedly this isn't very useful as the `api` of each component is empty. Additionally the `function` doesn't do anything other than `return` this empty `api`. We'll resolve this shortly.

Since we already completed the *Change* and *Depend* substeps we can use their results to inform our component design. We will use [Zoom Level 1](/80-20-javascript/#zoom-level-1-scope-tree) to do so as we don't need to worry about the implementation of the code yet. Only then would zoom levels two and three become useful.

#### Interface

Once we have our component shells, we need to think through the API of each component. Put another way, we need to define how the components:
- Manage their state
    - Get updated values
    - Set updated values
- Communicate state changes
    - Dispatch events and/or values

It's worth noting that frameworks such as [Vue.js](https://vuejs.org/), [React](https://reactjs.org/), and [Angular](https://angular.io/) exist to help with massively simplifying and easing the component creation, destruction, and state management processes of applications both small and large. I highly recommend using one of them in the future, but the scope of this book is to communicate how to use 80/20 JavaScript in its vanilla form. Having a deeper understanding of coding generally and JavaScript specifically will only help you when using frameworks in the future.

Onward. The first step we can take is by identifying certain HTML nodes so that we can talk to them with JavaScript. As you may recall from earlier the `id` attribute helps us here. So our HTML that was:

```html
<p class='empty-space-grow content-tweet-text'>What's happening?</p>
```

becomes:
```html
<p id='tweet-text' class='empty-space-grow content-tweet-text'>
    What's happening?
</p>
```

We do this for each node that we want to update at execution time, trigger events on, and/or listen to events from. This results in the `id='progress-bar'` and `id='tweet-button'` additions as well.

With this effort we can update our components to each have an approach for getting the reference to their element of interest. Remember, each element's corresponding JavaScript object has *its own API*. We will use this fact in the future to get more done with less.

```javascript
function TweetText() {
    var element = document.getElementById('tweet-text');
    var api = {};

    return api;
}

function ProgressBar() {
    var element = document.getElementById('progress-bar');
    var api = {};

    return api;
}

function TweetButton() {
    var element = document.getElementById('tweet-button');
    var api = {};

    return api;
}
```

Using the work from the previous substeps we can iterate on our components by starting to define the `function` and `var`iable names to associate with the input triggers and state respectively.

This is just a first pass. Coding is an iterative process as you may recall from [Refactor Early and Often](/80-20-javascript/#5-refactor-early-and-often):

> It can even be difficult to name identifiers well (believe it or not this is one of the more difficult aspects of programming). Thankfully the code is not set in stone. It is easy to move, change, and rename. This is refactoring.

With this in mind we can update our existing change list by appending proposed `function` and `var`iable names.

`TweetText`:

- Focused vs. Unfocused
    - Focused `isFocused` (assigned `true`)
        - User interaction (click/tap in) `onFocusIn()`
        - User interaction (tab keyboard in) `onFocusIn()`
    - Unfocused `isFocused` (assigned `false`)
        - User interaction (click/tap out) `onFocusOut()`
        - User interaction (tab keyboard out) `onFocusOut()`
    - Visible `isVisible` (assigned `true`)
        - Derived (from Tweet text `String` length)
    - Invisible `isVisible` (assigned `false`)
        - Derived (from Tweet text `String` length)
- Visible vs. Invisible
    - Visible `isVisible` (derived `true`)
        - Derived (from Tweet text `String` length) `text` of `TweetText`
    - Invisible `isVisible` (derived `false`)
        - Derived (from Tweet text `String` length) `text` of `TweetText`
- Tweet text        
    - Zero characters `text` (no input)
        - User interaction (key down/up) `onInputChange()`
        - User interaction (cut/paste) `onInputChange()`
    - One character `text` (one input)
        - User interaction (key down/up) `onInputChange()`
        - User interaction (cut/paste) `onInputChange()`
    - Two characters `text` (two inputs)
        - User interaction (key down/up) `onInputChange()`
        - User interaction (cut/paste) `onInputChange()`
    - Etc. `text` (you get it)
        - User interaction (key down/up) `onInputChange()`
        - User interaction (cut/paste) `onInputChange()`

`ProgressBar`:

- Completion percentage
    - Zero percent `percent` (0/280 characters)
        - Derived (from Tweet text `String` length) `onInputChange()` of `TweetText`
    - One percent `percent` (3/280 characters)
        - Derived (from Tweet text `String` length) `onInputChange()` of `TweetText`
    - Two percent `percent` (6/280 characters)
        - Derived (from Tweet text `String` length) `onInputChange()` of `TweetText`
    - Etc. `percent` (you get it)
        - Derived (from Tweet text `String` length) `onInputChange()` of `TweetText`

`TweetButton`:

- Enabled vs. Disabled
    - Enabled `isEnabled` (derived `true`)
        - Derived (from Tweet text `String` length) `text` of `TweetText`
    - Disabled `isEnabled` (derived `false`)
        - Derived (from Tweet text `String` length) `text` of `TweetText`

After this first pass, we can update our components using the `function` and `var`iable names we identified.

```javascript
function TweetText() {
    var isFocused;
    var isVisible; // Derived via `text` of `TweetText`. We will learn to do this later.
    var text;
    var element = document.getElementById('tweet-text');
    var api = {}
    
    function onFocusIn() {}
    function onFocusOut() {}
    function onInputChange() {}

    return api;
}

function ProgressBar() {
    var percent; // Derived via `onInputChange()` of `TweetText`. We will learn to do this later.
    var element = document.getElementById('progress-bar');
    var api = {};

    return api;
}

function TweetButton() {
    var isEnabled; // Derived via `text` of `TweetText`. We will learn to do this later.
    var element = document.getElementById('tweet-button');
    var api = {};

    return api;
}
```

And with that we have the interfaces for our components defined. They will likely change as we iterate but they are in a great spot for us to start our *implementation*.

#### Implementation

APIs let us do more with less. These APIs can be provided by the language, environment, or a third-party (including ourselves). In any case, they allow us to do more with less.

As you continue to learn what is possible in HTML, CSS, and JavaScript, you can prevent reinventing the wheel through your knowledge of APIs. You don't need to memorize them or anything, but it helps *to know they exist*. Now is a good time to reflect on a recommendation from earlier in the [Non-Reserved Keywords - Environment](/80-20-javascript/#non-reserved-keywords-environment) section:

> I do however recommend exploring the list of [all the web APIs](https://developer.mozilla.org/en-US/docs/Web/API) sometime. The effort enables you to grasp the big picture of what is possible by default in the browser.

This recommendation was made in the context of JavaScript, but the same holds true for HTML and CSS. There are numerous [HTML element types](https://developer.mozilla.org/en-US/docs/Web/HTML/Element) and [CSS properties](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference) that are truly empowering. In the context of our Tweet Box UI for example we can and will step outside our 80/20 element coverage as the `<textarea>` element is great for allowing a user to edit text like a Tweet.

It's worth mentioning that the `<textarea>` will fall a bit short of the actual Tweet Box text box. This is the case because it does not support nested elements like `<span>`s and `<a>` that Twitter uses to properly implement at tags (`@`) and hashtags (`#`). If there is enough interest in this complex implementation I will add a bonus chapter of advanced HTML, CSS, and JavaScript to this book. 

For now, the `<textarea>` will get us 80/20 of the features with minimal effort. Our update will consist of simply swapping out the element type and adding a `placeholder` attribute.

```html
<p id='tweet-text' class='empty-space-grow content-tweet-text'>
    What's happening?
</p>
```

becomes:

```html
<textarea
    id='tweet-text'
    class='empty-space-grow content-tweet-text'
    placeholder="What's happening?"></textarea>
```

An additional CSS update makes it so the `<textarea>` can grow its `height` automatically, prevent manual `resize`ing, and visually remove an undesirable `border` and `outline`.

```css
.content-tweet-box {
  height: 100%;
  /* Other styles not shown for brevity */
}

.content-tweet-text {
  border: none;
  outline: none;
  resize: none;
  /* Other styles not shown for brevity */
}
```

With just these small changes, we can now add, edit, cut, copy, paste, or delete text. Nice!

<code-sandbox-tweet-box-with-textarea-nog13 label="Tweet Box with Textarea">

With this addition of the `<textarea>` we can now make another pass on our interface and implementation by leveraging its text changes to derive updates for our components. The first iterative update is to encapsulate our three components in a new component. We do this to better manage, nest, and communicate code relationships. Since the `TweetText`, `ProgressBar`, and `TweetButton` can be grouped and parented by a new `TweetBox` component, that's what we'll do. We will do so using our previously used interface substep approach:

```javascript
function TweetBox() {
    var tweetText;
    var progressBar;
    var tweetButton;
    var api = {};

    function TweetText() {
        var isFocused;
        var isVisible; // Derived via `text` of `TweetText`. We will learn to do this later.
        var text;
        var element = document.getElementById('tweet-text');
        var api = {}
        
        function onFocusIn() {}
        function onFocusOut() {}
        function onInputChange() {}

        return api;
    }

    function ProgressBar() {
        var percent; // Derived via `onInputChange()` of `TweetText`. We will learn to do this later.
        var element = document.getElementById('progress-bar');
        var api = {};

        return api;
    }

    function TweetButton() {
        var isEnabled; // Derived via `text` of `TweetText`. We will learn to do this later.
        var element = document.getElementById('tweet-button');
        var api = {};

        return api;
    }

    return api;
}
```

If you have been paying close attention, you may have realized that although we're defining our interface and implementation, the code in our functions won't actually run yet. This is because none of our `function`s have *been called*. Let's fix this now:

```javascript
function TweetBox() {
    var tweetText = new TweetText();
    var progressBar = new ProgressBar();
    var tweetButton = new TweetButton();
    var api = {};

    function TweetText() {
        var isFocused;
        var isVisible; // Derived via `text` of `TweetText`. We will learn to do this later.
        var text;
        var element = document.getElementById('tweet-text');
        var api = {}
        
        function onFocusIn() {}
        function onFocusOut() {}
        function onInputChange() {}

        return api;
    }

    function ProgressBar() {
        var percent; // Derived via `onInputChange()` of `TweetText`. We will learn to do this later.
        var element = document.getElementById('progress-bar');
        var api = {};

        return api;
    }

    function TweetButton() {
        var isEnabled; // Derived via `text` of `TweetText`. We will learn to do this later.
        var element = document.getElementById('tweet-button');
        var api = {};

        return api;
    }

    return api;
}

var tweetBox = new TweetBox();
```

Now all four functions run and thus their code gets executed. Still not much happens outside various `var`iable assignments. Since the `api`s of each are all empty, not much is going on in the application. Let's change that.

As we've previously defined, we have input triggers that we want to listen to. In order to do this, we'll update `TweetText`'s `api` so that its useful to the other components. Now is the time to starting thinking in zoom level 2 and 3.

```javascript
function TweetBox() {
    var tweetText = new TweetText();
    var progressBar = new ProgressBar();
    var tweetButton = new TweetButton();
    var api = {};

    tweetText.element.addEventListener('input', onTweetTextInput);

    function onTweetTextInput(event) {
        console.log('The textarea change is: ' + event.data);
    }

    function TweetText() {
        var isFocused;
        var isVisible; // Derived via `text` of `TweetText`. We will learn to do this later.
        var text;
        var element = document.getElementById('tweet-text');
        var api = { element: element }
        
        function onFocusIn() {}
        function onFocusOut() {}
        function onInputChange() {}

        return api;
    }

    function ProgressBar() {
        var percent; // Derived via `onInputChange()` of `TweetText`. We will learn to do this later.
        var element = document.getElementById('progress-bar');
        var api = {};

        return api;
    }

    function TweetButton() {
        var isEnabled; // Derived via `text` of `TweetText`. We will learn to do this later.
        var element = document.getElementById('tweet-button');
        var api = {};

        return api;
    }

    return api;
}

var tweetBox = new TweetBox();
```

Take note of the `onTweetTextInput` and that it gets called on the input trigger. Right now we're only outputting the change to the `console`, but as we know from our previous substeps, we want to use this information to `derive` values for our `ProgressBar` and `TweetButton`. To do this we'll focus on `onTweetTextInput`s implementation and additionally update the `api`s of both the `ProgressBar` and `TweetButton`.

```javascript
function onTweetTextInput(event) {
    var allTheText = event.target.value;
    progressBar.update(allTheText);
    tweetButton.update(allTheText);
}
```

As a result of this implementation, we additionally need `update` implementations. The iterative updates result in:

```javascript
function TweetBox() {
    var tweetText = new TweetText();
    var progressBar = new ProgressBar();
    var tweetButton = new TweetButton();
    var api = {};

    tweetText.element.addEventListener('input', onTweetTextInput);

    function onTweetTextInput(event) {
        var allTheText = event.target.value;
        progressBar.update(allTheText);
        tweetButton.update(allTheText);
    }

    function TweetText() {
        var isFocused;
        var text;
        var element = document.getElementById('tweet-text');
        var api = { element: element }
        
        function onFocusIn() {}
        function onFocusOut() {}
        function onInputChange() {}

        return api;
    }

    function ProgressBar() {
        var percent; // Derived via `onInputChange()` of `TweetText`. We will learn to do this later.
        var element = document.getElementById('progress-bar');
        var api = { update: update };

        function update(text) {
            console.log('Derive percent using text argument value');
        }

        return api;
    }

    function TweetButton() {
        var isEnabled; // Derived via `text` of `TweetText`. We will learn to do this later.
        var element = document.getElementById('tweet-button');
        var api = { update: update };

        function update(text) {
            console.log('Derive isEnabled using text argument value');
        }

        return api;
    }

    return api;
}

var tweetBox = new TweetBox();
```

Since updating to our `<textarea>` element, we don't need to derive the `isVisible` state of the `TweetText` as that functionality is built-in to it. When implementing the actual Twitter Tweet Box component, we'd still have to manage this state however. For now, we do not and that's why it was removed in the snippet above.

Now let's replace our temporary `console.log` statement with our desired `update` implementation in the `ProgressBar`.

```javascript
function update(text) {
    var valueWithDecimal = (text.length / 280) * 100;
    percent = Math.ceil(valueWithDecimal);

    render();
}
```

It's the `TweetButton`'s turn.

```javascript
function update(text) {
    var isAboveMinimum = text.length > 0;
    var isBelowMaximum = text.length < 281
    isEnabled = isAboveMinimum && isBelowMaximum;
    
    render();
}
```

Since we want these updates to be communicated visually, we also need to tell the browser. We do this with a `render` `function` that leverages its component's `element` reference in some way. If a component doesn't need to be reflected visually, then it wouldn't need a reference to an element. Just something to think about when you are defining components in the future.

For the `ProgressBar` we take a shortcut in the `render` `function` and display text instead of updating the SVG ring. I'll leave this as a challenge to explore. Here is the component in its entirety:

```javascript
function ProgressBar() {
    var percent;
    var element = document.getElementById('progress-bar');
    var api = { update: update };

    function update(text) {
        var valueWithDecimal = (text.length / 280) * 100;
        percent = Math.ceil(valueWithDecimal);

        render();
    }

    function render() {
        element.innerText = percent;
    }

    return api;
}
```

For the `TweetButton` we add the familiar `halve-opacity` CSS class and either `add` or `remove` it at execution time. Here is the component in its entirety:

```javascript
function TweetButton() {
    var isEnabled;
    var element = document.getElementById('tweet-button');
    var api = { update: update };

    function update(text) {
        var isAboveMinimum = text.length > 0;
        var isBelowMaximum = text.length < 281
        isEnabled = isAboveMinimum && isBelowMaximum;

        render();
    }

    function render() {
        element.disabled = !isEnabled;
        
        if(isEnabled) {
            element.classList.remove('halve-opacity');
        } else {
            element.classList.add('halve-opacity');
        }
    }

    return api;
}
```

With these two component updates we've iterated our design closer toward Twitter's actual experience. Here is the updated interactive sample:

<code-sandbox-tweet-box-using-derived-values-m034z label="Tweet Box Using Derived Values">

Though we could continue iterating to gradually make our 80/20 `TweetBox` component more robust and feature complete, we will stop here.

Throughout this chapter we learned a lot about deconstructing and reconstructing designs. In addition to defining and exploring a generalized step-by-step process, we used it in practice against the Twitter Tweet Box UI. It is important to stress that this same process is useful in not only existing designs, but the ones you dream up and manifest as you grow as a designer and coder.

Thank you for reading and good luck on your continued journey!
