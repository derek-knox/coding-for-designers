## Reconstruct Style


This process consists of three core substeps:
1. Placeholder - *Setup placeholder styles to help visualize our grid*
1. Layout - *Setup layout styles*
    - Children - *Child rectangle focus*
    - Space - *Empty space focus*
1. Content - *Setup content styles*

### Placeholder

Since we already did the work of identifying the depth of each node, we can simply convert our various "(depth X)" notes into `class`es. For example:

```html
<div> main (depth 1) (children 2)
```

becomes:

```html
<div class='depth-1'> main (children 2)
```

Before manually updating each node, I highly recommend looking into a code editor that supports *multiple cursors*. Welcome to a superpower. It took only *twenty seconds* to update the HTML tree with the `class` definition at each node using multiple cursors. Coders like shortcuts remember? As you may recall from the [*Style*](/structure-style-and-behavior/#style) section:

> A professional’s level of experience simply makes him or her quicker in their application of it. Professional coders—just like beginners—still need to reference resources.

We refresh on this for two reasons:
1. Experience manifests as tools knowledge, not just speed
1. Style resource referencing was needed to complete this section and this book!

The multiple cursors feature is perfect for facilitating this `class='depth-x'` update efficiently, but the manual approach works well too. Regardless of the approach, the full update looks like this:

```html
<div class='depth-1'> main (children 2)
    <div class='depth-2'> avatar (children 1)
        <img class='depth-3'> avatar graphic (children 0) </img>
    </div>
    <div class='depth-2'> actions (children 2)
        <div class='depth-3'> top (children 1)
            <p class='depth-4'> What's happening? text (children 0) </p>
        </div>
        <div class='depth-3'> bottom (children 2)
            <div class='depth-4'> left (children 5)
                <button class='depth-5'> photo (children 1)
                    <img class='depth-6'> photo graphic (children 0) </img>
                </button>
                <button class='depth-5'> gif (children 1)
                    <img class='depth-6'> gif graphic (children 0) </img>
                </button>
                <button class='depth-5'> poll (children 1)
                    <img class='depth-6'> poll graphic (children 0) </img>
                </button>
                <button class='depth-5'> emoji (children 1)
                    <img class='depth-6'> emoji graphic (children 0) </img>
                </button>
                <button class='depth-5'> more... (children 1)
                    <img class='depth-6'> more... graphic (children 0) </img>
                </button>
            </div>
            <div class='depth-4'> right (children 4)
                <div class='depth-5'> progress bar (children 1)
                    <img class='depth-6'> progress bar graphic (children 0) </img>
                </div>
                <div class='depth-5'> divider (children 1)
                    <img class='depth-6'> divider graphic (children 0) </img>
                </div>
                <button class='depth-5'> add (children 1)
                    <img class='depth-6'> plus graphic (children 0) </img>
                </button>
                <button class='depth-5'> Tweet (children 1)
                    <span class='depth-6'> Tweet text (children 0) </span>
                </button>
            </div>
        </div>
    </div>
</div>
```

After updating all our HTML element nodes with the proper `class` placeholders, we simply need to update our CSS so that we actually define each of these placeholder styles. The color choice for each placeholder is up to you. The only goal influencing color choice is to ensure they are clearly differentiated. Since we have six depths and there are seven distinct colors in the rainbow, we'll start with those:

```css
.depth-1 { background-color: red;     /* #FF0000 */ }
.depth-2 { background-color: orange;  /* #FFA500 */ }
.depth-3 { background-color: yellow;  /* #FFFF00 */ }
.depth-4 { background-color: green;   /* #00FF00 */ }
.depth-5 { background-color: blue;    /* #0000FF */ }
.depth-6 { background-color: indigo;  /* #4B0082 */ }
```

Now that each depth container is clearly differentiated by color, we can now more easily see our style updates in the browser as we begin to iterate toward our layout and content CSS styles.

#### Layout - Children

Now we want to fix our layout. Put another way, we need to properly *distribute the children and space within the parent rectangles*. The `class` attribute enables this.

We'll focus on the child rectangles to start, so we'll use either `layout-horizontal` or `layout-vertical`. An update is needed only if the child count is two or more. For example:

```html
<div class='depth-1'> main (children 2)
```

updates to:

```html
<div class='depth-1 layout-horizontal'> main (children 2)
```

but:

```html
<div class='depth-2'> avatar (children 1)
```

remains the same. When we update our entire tree we get:

```html
<div class='depth-1 layout-horizontal'> main (children 2)
    <div class='depth-2'> avatar (children 1)
        <img class='depth-3'> avatar graphic (children 0) </img>
    </div>
    <div class='depth-2 layout-vertical'> actions (children 2)
        <div class='depth-3 layout-horizontal'> top (children 1)
            <p class='depth-4'> What's happening? text (children 0) </p>
        </div>
        <div class='depth-3 layout-horizontal'> bottom (children 2)
            <div class='depth-4 layout-horizontal'> left (children 5)
                <button class='depth-5'> photo (children 1)
                    <img class='depth-6'> photo graphic (children 0) </img>
                </button>
                <button class='depth-5'> gif (children 1)
                    <img class='depth-6'> gif graphic (children 0) </img>
                </button>
                <button class='depth-5'> poll (children 1)
                    <img class='depth-6'> poll graphic (children 0) </img>
                </button>
                <button class='depth-5'> emoji (children 1)
                    <img class='depth-6'> emoji graphic (children 0) </img>
                </button>
                <button class='depth-5'> more... (children 1)
                    <img class='depth-6'> more... graphic (children 0) </img>
                </button>
            </div>
            <div class='depth-4 layout-horizontal'> right (children 4)
                <div class='depth-5'> progress bar (children 1)
                    <img class='depth-6'> progress bar graphic (children 0) </img>
                </div>
                <div class='depth-5'> divider (children 1)
                    <img class='depth-6'> divider graphic (children 0) </img>
                </div>
                <button class='depth-5'> add (children 1)
                    <img class='depth-6'> plus graphic (children 0) </img>
                </button>
                <button class='depth-5'> Tweet (children 1)
                    <span class='depth-6'> Tweet text (children 0) </span>
                </button>
            </div>
        </div>
    </div>
</div>
```

After this initial layout pass our rendered structure begins to look closer to our goal. It is still quite a ways off however.

<code-sandbox-style-with-layout-children-placeholders-xgpp7 label="Style with Layout Children Placeholders">


Our various `depth-X` classes combined with our added `layout-horizontal` and `layout-vertical` class definitions enable the above render. The class definitions are:

```css
.layout-horizontal {
    display: flex;
    flex-direction: row;
}

.layout-vertical {
    display: flex;
    flex-direction: column;
}
```

Since we don't need our children count information anymore, we can further cleanup our layout styling. To do so we'll simply remove all our notes that helped us up to this point. If our end design uses specific text then we'll leave that text. For example:

```html
<p class='depth-4'> What's happening? text (children 0) </p>
```

becomes:

```html
<p class='depth-4'>What's happening?</p>
```

and:

```html
<span class='depth-6'> Tweet text (children 0) </span>
```

becomes:

```html
<span class='depth-6'>Tweet</span>
```

Take note that we also cleanup the spacing between each element's opening and closing tags. Our end result becomes:

```html
<div class='depth-1 layout-horizontal'>
    <div class='depth-2'>
        <img class='depth-3'></img>
    </div>
    <div class='depth-2 layout-vertical'>
        <div class='depth-3 layout-horizontal'>
            <p class='depth-4'>What's happening?</p>
        </div>
        <div class='depth-3 layout-horizontal'>
            <div class='depth-4 layout-horizontal'>
                <button class='depth-5'>
                    <img class='depth-6'></img>
                </button>
                <button class='depth-5'>
                    <img class='depth-6'></img>
                </button>
                <button class='depth-5'>
                    <img class='depth-6'></img>
                </button>
                <button class='depth-5'>
                    <img class='depth-6'></img>
                </button>
                <button class='depth-5'>
                    <img class='depth-6'></img>
                </button>
            </div>
            <div class='depth-4 layout-horizontal'>
                <div class='depth-5'>
                    <img class='depth-6'></img>
                </div>
                <div class='depth-5'>
                    <img class='depth-6'></img>
                </div>
                <button class='depth-5'>
                    <img class='depth-6'></img>
                </button>
                <button class='depth-5'>
                    <span class='depth-6'>Tweet</span>
                </button>
            </div>
        </div>
    </div>
</div>
```

When viewing this tree rendered in the browser, it may seem like we took a step backward. It appears this way because we have yet to define *empty space styles* for our containers.

#### Layout - Space

We'll start by adding a placeholder style addition of `padding: 5px; margin: 5px;` to each `depth-X` class definition. When rendered now, we see that we are still on the right track.

<code-sandbox-style-with-layout-space-placeholders-6rdrs label="Style with Layout Space Placeholders">

Let's leave this `padding: 5px; margin: 5px;` placeholder as it improves visualizing our progress as we iterate.

To finish off our layout step we need a few of our containers to properly handle *empty space*. Similar to how our `layout-horizontal` and `layout-vertical` styles help layout *child rectangles*, we also need styles to layout *empty space*. The first will be used to tell a container how to *grow into* empty space and the second and third will tell a container how to *distribute* empty space. The class names we'll use are:

- `empty-space-grow`
- `empty-space-between`
- `empty-space-around`

Based off these class names and the [Style with Layout Space Placeholders](#style-with-layout-space-placeholders) render from above, try to guess which containers need them. Here is a clue:

- `empty-space-grow` (two instances)
- `empty-space-between` (one instance)
- `empty-space-around` (nine instances)

Combined, they result in this render:

<code-sandbox-style-with-layout-space-tf846 label="Style with Layout Space">

There are two core changes needed to achieve this result. The first being:

```html
<!-- The above code removed for brevity -->
<div class='depth-2 layout-vertical empty-space-grow'>
    <div class='depth-3 layout-horizontal'>
        <p class='depth-4 empty-space-grow'>What's happening?</p>
    </div>
    <div class='depth-3 layout-horizontal empty-space-between'>
<!-- The below code removed for brevity -->
```

The second core change will also include a [Layout - Children](#layout-children) style addition. We learned previously that an update is needed, "...only if the child count is two or more." This is still the correct default rule to follow. As seen in the rendered output however, it becomes obvious that an iterative update is required to vertically center some of the content. Naturally, the progress bar, divider, and all `<button>` elements get this update. The result becomes:

```html
<div class='depth-1 layout-horizontal'>
    <div class='depth-2'>
        <img class='depth-3'></img>
    </div>
    <div class='depth-2 layout-vertical empty-space-grow'>
        <div class='depth-3 layout-horizontal'>
            <p class='depth-4 empty-space-grow'>What's happening?</p>
        </div>
        <div class='depth-3 layout-horizontal empty-space-between'>
            <div class='depth-4 layout-horizontal'>
                <button class='depth-5 layout-vertical empty-space-around'>
                    <img class='depth-6'></img>
                </button>
                <button class='depth-5 layout-vertical empty-space-around'>
                    <img class='depth-6'></img>
                </button>
                <button class='depth-5 layout-vertical empty-space-around'>
                    <img class='depth-6'></img>
                </button>
                <button class='depth-5 layout-vertical empty-space-around'>
                    <img class='depth-6'></img>
                </button>
                <button class='depth-5 layout-vertical empty-space-around'>
                    <img class='depth-6'></img>
                </button>
            </div>
            <div class='depth-4 layout-horizontal'>
                <div class='depth-5 layout-vertical empty-space-around'>
                    <img class='depth-6'></img>
                </div>
                <div class='depth-5 layout-vertical empty-space-around'>
                    <img class='depth-6'></img>
                </div>
                <button class='depth-5 layout-vertical empty-space-around'>
                    <img class='depth-6'></img>
                </button>
                <button class='depth-5 layout-vertical empty-space-around'>
                    <span class='depth-6'>Tweet</span>
                </button>
            </div>
        </div>
    </div>
</div>
```

The `class` definitions for these additions are:

```css
.empty-space-around {
    justify-content: space-around;
}

.empty-space-between {
    justify-content: space-between;
}

.empty-space-grow {
    flex-grow: 1;
}
```

The step order in this section is intentional and gets you at least an 80/20 result. It's important to remember that the closer we get to the content step, the more we'll have to tweak our initial structure and style to manifest our target design.

Now that we are finished with our [Layout - Space](#layout-space) step, we can finally move on to design *content*.

#### Content

With our placeholder and layout styling in place, we now have a great baseline to iterate on. In comparing our current render with our target design, the next major differences relate to imagery. Or to be more precise, the lack of imagery. As you may recall from the [*Constructs and Components*](/programming-and-visual-design/#constructs-and-components) section there are three categories of imagery:
- Photography
- Iconography
- Illustration

We can organize our updates using these categories:

- Photography
    - Avatar
- Iconography
    - Photo
    - Gif
    - Poll
    - Emoji
    - More...
    - Divider
    - Add
- Illustration
    - Progress Bar

Put another way, we simply need to update all the `<img>` element tags with their correct `src` attribute and value. As designers we know that `.jpeg`, `.png`, and `.gif` are common raster formats (useful in photography) where `.svg` is the most common vector format (useful in iconography). Illustrations often straddle this line and since we have to update the Progress Bar at execution time, we'll use a placeholder `.svg` version until the final [Reconstruct Behavior](#reconstruct-behavior) step.

As we learned how to link to asset files in [*Anatomy of HTML, CSS, and JavaScript
*](/interactive-code/#anatomy-of-html-css-and-javascript) we simply update each `src` attribute to point to the desired image file. We end up with the following render:

<code-sandbox-style-content-with-imagery-and-placeholders-9jjco label="Style Content with Imagery and Placeholders">

Now to improve our content style, we'll add two `class`es to enforce the desired `width` and `height` dimensions of some content. The numbers below are not made up. They were confirmed on Twitter's website via the browser's developer tools that were first mentioned in the [*Debugger Statement*](/80-20-javascript/#debugger-statement) section. You could easily create a bounding box rectangle in your design tool of choice to come to the same conclusions.

In both Twitter's code and your design tool of choice, the units are in *pixels*. We will instead use `rem` (*root element font-size*) units. Why? As you may recall from the [Deconstruct](#deconstruct) section:

> Thinking and designing for dynamic—not static—layouts is the single biggest step to leveling up as a designer.

This means we need to think in *percentages and ratios vs. exact pixel dimensions*. The `rem` unit enables this. There are online conversion tools to help you convert from `px` to `rem` and vice-versa. A common base `font-size: 16px` is `1rem` for example and the one we use. Using `rem`, our added styles are:

```css
.graphic-avatar {
    border-radius: 50%;
    height: 3rem;
    width: 3rem;
}
```

We'll additionally add some button related classes to ensure we override the *browser default styles* first mentioned in the [*Style*](/structure-style-and-behavior/#style) section. We update each `<button>` with the `button` class, selectively apply the `button-tweet-media` class, and apply the `button-tweet` to the Tweet button:

```css
.button {
    background-color: transparent;    
    border: 0;
    cursor: pointer;
    padding: 0;
}

.button-tweet-media {
    margin: 0.25rem 0.4rem;
}

.button-tweet {
    background-color: #1DA1F2;
    border-radius: 2rem;
    color: #FFFFFF;
    height: 2.5rem;
    margin-left: 0.5rem;
}
```

With the above content style updates we get the following render:

<code-sandbox-style-content-with-placeholders-1pobo label="Style Content with Placeholders">

If we temporarily remove our placeholder styles we get:

<code-sandbox-style-content-without-placeholders-2xj43 label="Style Content without Placeholders">

We're so close! We only have minor content spacing styles left to apply. Since this is our focus, we no longer need our `padding: 5px; margin: 5px;` helpers. Once removed, our render becomes:

<code-sandbox-style-content-with-color-placeholders-only-47yk2 label="Style Content with Color Placeholders Only">

We can see that there are a few target areas in need of content spacing:
- Tweet Box
- "What's happening?" text
- Tweet text

The following `class`es get us 99% of what we want style-wise:

```css
.content-tweet-box {
    padding: 0.5rem 1rem; 
}

.content-tweet-text {
    color: grey;
    font-size: 1.4rem;
    font-weight: 300;
    margin: 0.75rem 0.5rem 2rem 1rem;
}

.content-tweet-label {
    font-size: 1rem;
    font-weight: 600;
    margin: 0 1rem 0 1rem;
}
```

When we completely remove our placeholders our structure becomes:

```html
<div class='layout-horizontal content-tweet-box'>
    <div>
        <img class='graphic-avatar' src='assets/img/avatar.png'></img>
    </div>
    <div class='layout-vertical empty-space-grow'>
        <div class='layout-horizontal'>
            <p class='empty-space-grow content-tweet-text'>What's happening?</p>
        </div>
        <div class='layout-horizontal empty-space-between'>
            <div class='layout-horizontal'>
                <button class='layout-vertical empty-space-around button button-tweet-media'>
                    <img src='assets/img/photo.svg'></img>
                </button>
                <button class='layout-vertical empty-space-around button button-tweet-media'>
                    <img src='assets/img/gif.svg'></img>
                </button>
                <button class='layout-vertical empty-space-around button button-tweet-media'>
                    <img src='assets/img/poll.svg'></img>
                </button>
                <button class='layout-vertical empty-space-around button button-tweet-media'>
                    <img src='assets/img/emoji.svg'></img>
                </button>
                <button class='layout-vertical empty-space-around button button-tweet-media'>
                    <img src='assets/img/more.svg'></img>
                </button>
            </div>
            <div class='layout-horizontal'>
                <div class='layout-vertical empty-space-around'>
                    <img src='assets/img/progress-bar.svg'></img>
                </div>
                <div class='layout-vertical empty-space-around'>
                    <img src='assets/img/divider.svg'></img>
                </div>
                <button class='layout-vertical empty-space-around button'>
                    <img src='assets/img/add.svg'></img>
                </button>
                <button class='layout-vertical empty-space-around button button-tweet'>
                    <span class='content-tweet-label'>Tweet</span>
                </button>
            </div>
        </div>
    </div>
</div>
```

And our style becomes:

```css
/* Layout - Children */

.layout-horizontal {
    display: flex;
    flex-direction: row;
}

.layout-vertical {
    display: flex;
    flex-direction: column;
}

/* Layout - Space */

.empty-space-around {
    justify-content: space-around;
}

.empty-space-between {
    justify-content: space-between;
}

.empty-space-grow {
    flex-grow: 1;
}

/* Layout - Content */

.graphic-avatar {
    border-radius: 50%;
    height: 3rem;
    width: 3rem;
}

.button {
    background-color: transparent;
    border: 0;
    cursor: pointer;
    padding: 0;
}

.button-tweet-media {
    margin: 0.25rem 0.4rem;
}

.button-tweet {
    background-color: #1DA1F2;
    border-radius: 2rem;
    color: #FFFFFF;
    height: 2.5rem;
    margin-left: 0.5rem;
}

.content-tweet-box {
    padding: 0.5rem 1rem;
}

.content-tweet-text {
    color: grey;
    font-size: 1.4rem;
    font-weight: 300;
    margin: 0.75rem 0.5rem 2rem 1rem;
}

.content-tweet-label {
    font-size: 1rem;
    font-weight: 600;
    margin: 0 1rem 0 1rem;
}
```

Using these structure and style definitions, we get a Tweet Box whose layout is responsive and flexible unlike Twitter's constrained Tweet Box:

<code-sandbox-style-without-placeholders-68218 label="Style without Placeholders">

In order to get our final target render to match Twitter, we simply need to *constrain* it by updating our `content-tweet-box` class. Updating it from:

```css
.content-tweet-box {
    padding: 0.5rem 1rem;
}
```

to:

```css
.content-tweet-box {
    background-color: #FFFFFF;
    padding: 0.5rem 1rem;
    height: 118px;
    width: 598px;
}
```

while additionally changing the `background-color` of the entire `body` with:

```css
body {
    background-color: #EEEEEE;
}
```

finally gives us our target render:

<code-sandbox-target-render-u37h6 label="Target Render">

For comparison, here is the original screenshot of Twitter's Tweet Box UI that we referenced at the beginning of this chapter:

![Tweet Box UI Reference](../assets/img/visuals/twitter-screenshot.png?v1.2.39 "Tweet Box UI Reference")

Our HTML and CSS solutions are different from the original Tweet UI code as we used our 80/20 subset, *but we get the same visual effect*. The above exercise:

1. Reinforces the power of 80/20
2. Exemplifies iteration and the [*Work. Right. Better.*](/interactive-code/#work-right-better-) approach
3. Provides a detailed step-by-step process for converting a static design into a dynamic and interactive one

We can go much further regarding its dynamic and interactive nature however. Only the browser defaults of responsive layout and button hover interactivity exists currently. We cannot type in the Tweet Box and and nothing else it interactive. This leads us to our final reconstruction step.
