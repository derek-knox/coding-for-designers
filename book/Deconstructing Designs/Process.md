## Process

Before advancing, it is worth previewing the specific process we'll use to deconstruct and reconstruct Twitter's Tweet UI. There are four overarching steps where each has a set of substeps. Each step and substep focuses on a *specific approach to solving a certain problem*. In aggregate, the entire process is used to convert a static design into a dynamic and interactive one.

There is no need to memorize the below steps and substeps. Their presence is solely to reveal a *repeatable and sequential process for converting a static design into a dynamic and interactive one*. Here is the full process:

1. Deconstruct - *Determine the grid and its cells*
    - Rectangle - *Determine the rectangle count*
    - Nest - *Determine the parent-child relationships*
    - Tree - *Create a tree structure using these facts*
1. Reconstruct Structure - *Recreate the structure in HTML*
    - Depth - *Determine each node's nesting depth*
    - Children - *Determine the child count at each node*
    - Grid - *Create the structural grid in code using these facts*
        - Bottom - *Start from the bottom*
        - Depth - *Start at the deepest depth*
        - Wrap - *Update each closing tag's position to wrap its children*
        - Repeat - *Finish each depth before repeating*
1. Reconstruct Style - *Recreate the style with CSS*
    - Placeholder - *Setup placeholder styles to help visualize our grid*
    - Layout - *Setup layout styles*
        - Children - *Child rectangle focus*
        - Space - *Empty space focus*
    - Content - *Setup content styles*
1. Reconstruct Behavior - *Recreate the behavior with JavaScript*
    - Change - *Determine what changes at execution time*
    - Depend - *Determine the change dependencies*
    - Component - *Create components using these facts*
        - Shell - *Code the component shell*
        - Interface - *Code the component interface*
        - Implementation - *Code the component implementation*
