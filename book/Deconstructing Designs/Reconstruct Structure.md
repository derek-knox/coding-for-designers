## Reconstruct Structure


Bonus points to you if you noticed the tree is nested six levels deep. This information is extremely valuable as it becomes our starting point for the reconstruction process. This process consists of three core substeps:

1. Depth - *Determine each node's nesting depth*
1. Children - *Determine the child count at each node*
1. Grid - *Create the structural grid in code using these facts*

### Depth

Building off our tree from the deconstruct process, we can update it with the depth information:

```html
<div> main (depth 1)
    <div> avatar (depth 2)
        <img> avatar graphic (depth 3)
    <div> actions (depth 2)
        <div> top (depth 3)
            <p> What's happening? text (depth 4)
        <div> bottom (depth 3)
            <div> left (depth 4)
                <button> photo (depth 5)
                    <img> photo graphic (depth 6)
                <button> gif (depth 5)
                    <img> gif graphic (depth 6)
                <button> poll (depth 5)
                    <img> poll graphic (depth 6)
                <button> emoji (depth 5)
                    <img> emoji graphic (depth 6)
                <button> more... (depth 5)
                    <img> more... graphic (depth 6)
            <div> right (depth 4)
                <div> progress bar (depth 5)
                    <img> progress bar graphic (depth 6)
                <div> divider (depth 5)
                    <img> divider graphic (depth 6)
                <button> add (depth 5)
                    <img> plus graphic (depth 6)
                <button> Tweet (depth 5)
                    <span> Tweet text (depth 6)
```

### Children

We can further update our tree with the child count information. Take note that we only count *immediate* children.

```html
<div> main (depth 1) (children 2)
    <div> avatar (depth 2) (children 1)
        <img> avatar graphic (depth 3) (children 0)
    <div> actions (depth 2) (children 2)
        <div> top (depth 3) (children 1)
            <p> What's happening? text (depth 4) (children 0)
        <div> bottom (depth 3) (children 2)
            <div> left (depth 4) (children 5)
                <button> photo (depth 5) (children 1)
                    <img> photo graphic (depth 6) (children 0)
                <button> gif (depth 5) (children 1)
                    <img> gif graphic (depth 6) (children 0)
                <button> poll (depth 5) (children 1)
                    <img> poll graphic (depth 6) (children 0)
                <button> emoji (depth 5) (children 1)
                    <img> emoji graphic (depth 6) (children 0)
                <button> more... (depth 5) (children 1)
                    <img> more... graphic (depth 6) (children 0)
            <div> right (depth 4) (children 4)
                <div> progress bar (depth 5) (children 1)
                    <img> progress bar graphic (depth 6) (children 0)
                <div> divider (depth 5) (children 1)
                    <img> divider graphic (depth 6) (children 0)
                <button> add (depth 5) (children 1)
                    <img> plus graphic (depth 6) (children 0)
                <button> Tweet (depth 5) (children 1)
                    <span> Tweet text (depth 6) (children 0)
```

### Grid

Now we have all the information we need to recreate our grid in code. In fact, because our tree has a proposed HTML element, we can simply close each element's tag. This will give us the structure we're looking for. Sweet! Now our tree looks like:

```html
<div> main (depth 1) (children 2) </div>
    <div> avatar (depth 2) (children 1) </div>
        <img> avatar graphic (depth 3) (children 0) </img>
    <div> actions (depth 2) (children 2) </div>
        <div> top (depth 3) (children 1) </div>
            <p> What's happening? text (depth 4) (children 0) </p>
        <div> bottom (depth 3) (children 2) </div>
            <div> left (depth 4) (children 5) </div>
                <button> photo (depth 5) (children 1) </button>
                    <img> photo graphic (depth 6) (children 0) </img>
                <button> gif (depth 5) (children 1) </button>
                    <img> gif graphic (depth 6) (children 0) </img>
                <button> poll (depth 5) (children 1) </button>
                    <img> poll graphic (depth 6) (children 0) </img>
                <button> emoji (depth 5) (children 1) </button>
                    <img> emoji graphic (depth 6) (children 0) </img>
                <button> more... (depth 5) (children 1) </button>
                    <img> more... graphic (depth 6) (children 0) </img>
            <div> right (depth 4) (children 4) </div>
                <div> progress bar (depth 5) (children 1) </div>
                    <img> progress bar graphic (depth 6) (children 0) </img>
                <div> divider (depth 5) (children 1) </div>
                    <img> divider graphic (depth 6) (children 0) </img>
                <button> add (depth 5) (children 1) </button>
                    <img> plus graphic (depth 6) (children 0) </img>
                <button> Tweet (depth 5) (children 1) </button>
                    <span> Tweet text (depth 6) (children 0) </span>
```

As you may have thought when reading the above update, this won't actually work. The reason is that our closing HTML element tags *do not wrap their children*. In order to wrap them properly, we need to use our depth and child count information. With this information we can follow the below sequence:
1. Bottom - *Start from the bottom*
1. Depth - *Start at the deepest depth*
1. Wrap - *Update each closing tag's position to wrap its children*
1. Repeat - *Finish each depth before repeating*

For example the:
```html
<button> Tweet (depth 5) (children 1) </button>
    <span> Tweet text (depth 6) (children 0) </span>
```

becomes:

```html
<button> Tweet (depth 5) (children 1)
    <span> Tweet text (depth 6) (children 0) </span>
</button>
```

During this process you simply need to pay attention to the child count of each node. If it is `0` then you can skip it. Otherwise you need to update the closing tag's position so that it wraps its child nodes. Simple as that. When complete the result is:

```html
<div> main (depth 1) (children 2)
    <div> avatar (depth 2) (children 1)
        <img> avatar graphic (depth 3) (children 0) </img>
    </div>
    <div> actions (depth 2) (children 2)
        <div> top (depth 3) (children 1)
            <p> What's happening? text (depth 4) (children 0) </p>
        </div>
        <div> bottom (depth 3) (children 2)
            <div> left (depth 4) (children 5)
                <button> photo (depth 5) (children 1)
                    <img> photo graphic (depth 6) (children 0) </img>
                </button>
                <button> gif (depth 5) (children 1)
                    <img> gif graphic (depth 6) (children 0) </img>
                </button>
                <button> poll (depth 5) (children 1)
                    <img> poll graphic (depth 6) (children 0) </img>
                </button>
                <button> emoji (depth 5) (children 1)
                    <img> emoji graphic (depth 6) (children 0) </img>
                </button>
                <button> more... (depth 5) (children 1)
                    <img> more... graphic (depth 6) (children 0) </img>
                </button>
            </div>
            <div> right (depth 4) (children 4)
                <div> progress bar (depth 5) (children 1)
                    <img> progress bar graphic (depth 6) (children 0) </img>
                </div>
                <div> divider (depth 5) (children 1)
                    <img> divider graphic (depth 6) (children 0) </img>
                </div>
                <button> add (depth 5) (children 1)
                    <img> plus graphic (depth 6) (children 0) </img>
                </button>
                <button> Tweet (depth 5) (children 1)
                    <span> Tweet text (depth 6) (children 0) </span>
                </button>
            </div>
        </div>
    </div>
</div>
```

If you were to view this structured HTML in a browser, the rendered output would admittedly look ugly and seemingly disorganized. In fact, it would look like this:

<code-sandbox-structure-with-depth-children-and-grid-wum5z label="Structure with Depth, Children, and Grid">

This looks horrible visually, but it is structurally correct. We will fix this aesthetic issue now by applying style.

Before advancing, take note of the "Open Sandbox" button in the example above. All remaining examples in this chapter have it too. Click or tap it to learn, explore, and edit the code yourself!
