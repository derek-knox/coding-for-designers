Using this chapter's title as a clue, what do all the interactive games, tools, and software we love have in common?

Though your answer may be correct, what I'm looking for is that they are all composed of *interconnected rectangles*. This is the case for 2D anyway where 3D products are composed of *interconnected boxes*. The former are typically referred to as *bounding rectangles* and the latter *bounding boxes*. We'll stay focused on 2D.

What this means is that during your various design phases you are—consciously or subconsciously—grouping content into rectangles. Put another way, you are creating a grid. In some grid cells you are creating another grid. And so on.

As you may recall from the [*Constructs and Components*](/programming-and-visual-design#constructs-and-components) section:

> A grid instantly provides the scaffolding, scaling, and patterns of space in which imagery and typography will live.

It is no secret that a grid is extremely useful in visual design. It is even more valuable when coding. In fact, the grid is the shared foundation of the deconstruct and reconstruct processes. We will use this to our advantage and organize this chapter into four parts:

1. Deconstruct
1. Reconstruct Structure
1. Reconstruct Style
1. Reconstruct Behavior

We need a design to apply this process to. This is where Twitter comes in.

### Twitter

Twitter, and specifically its Tweet Box UI, is a great candidate for deconstructing and reconstructing. This is the case as it bridges the gap between simple and complex.

![Tweet Box UI](../assets/img/visuals/twitter-screenshot.png?v1.2.39 "Tweet Box UI")

Below is the zoomed in Tweet Box UI. When viewing it, think through the following before advancing:
- How many rectangles is it comprised of?
- What is their nested structure?
- Where are their boundaries?

![Tweet Box UI Zoomed](../assets/img/visuals/twitter-tweet-ui-zoomed.png?v1.2.39 "Tweet Box UI Zoomed")

To reconstruct, we will use our 80/20 learnings from earlier in this book. We will not use the actual HTML, CSS, or JavaScript that Twitter uses. This approach is intentional and will reinforce the power of 80/20 code subsets. Naturally this will help us be productive while achieving more with less.

Even though we focus on deconstructing and reconstructing Twitter's Tweet Box UI in the rest of this chapter, the *exact same process works for your designs*.