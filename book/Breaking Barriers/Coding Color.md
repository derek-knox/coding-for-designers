## Coding Color

Thus far we have made our own example codes. One to represent the symbols in the English language and another to represent black and white images. We could continue this trend. Instead we will transition toward codes that already exist. The advantage is twofold:

1. Less work for us, we don’t need to reinvent the wheel
2. Using established codes empowers us to achieve more, faster

In the context of color we will explore the common codes RGB and HEX. The former is *red green blue* and the latter is *hexadecimal*. We will start with RGB.

### RGB

RGB is composed of three values (color channels). A red value, a green value, and a blue value. Combined they represent a specific color value. The literal color represented by a specific RGB coded value has already been decided. No work on our end. Nice. We simply rely on the various design applications we use to consistently represent colors in our digital designs. Figma, Sketch, Adobe XD, Photoshop, Illustrator, and Lightroom are a few examples of the hundreds, if not thousands, of authoring applications we depend on to consistently represent color.

RGB is represented in a few flavors too. Let's start with using a bit for each red, green, and blue value. We saw earlier in the *Three Bits* visual that we get eight states and sequences. Now we'll simply associate a color with each.

![3-Bit RGB](../assets/img/visuals/3-bit-rgb.png?v1.2.39 "3-Bit RGB")

Eight colors is a pretty limiting palette. What happens if we swap out the bits for bytes?

Holy shit.

RGB gets us *over 16 million* color values. That palette is enormous. This is an actual flavor of RGB called RGB24. Another is called RGB32 or RGBA, where "A" is an *alpha* (transparency) value. Just call them by their normal names RGB and RGBA.

Here are the same eight colors seen above in the *3-Bit RGB* visual, but with three bytes instead of bits.

![RGB](../assets/img/visuals/rgb-sample.png?v1.2.39 "RGB Sample")

You will notice the sample RGB colors above range from 0-255 for each byte’s value. If I invented the RGB code, 1-256 would be used instead. I did not invent it and instead we must use 0-255. Sad.

Counting starting at zero instead of one is a recurring pattern you will see in coding. This is one of the fundamental aspects that throws non-coders off when first learning more about coding. We have started counting from *one not zero our entire lives*. This is admittedly a difficult pill to swallow, but we must. Thankfully it will become second nature in time. Feel free to pursue clarity through *zero-based numbering* research however. For a quick and dirty answer, math nerds and computer optimization are to blame.

### HEX

Hexadecimal is another common color code. It represents the same color range as RGB, just differently. Instead of a range between *0-255* for each R, G, and B value, HEX uses *00-FF* for each. The letters A-F replace the numbers 10-15 (A instead of 10 through F instead of 15).

So for HEX, each color channel is represented by *two* characters (00-FF) instead of RGB's *three* (0-255). A minimalist designer can appreciate this. So too can an efficient coder.

![HEX Chart](../assets/img/visuals/hex-chart.png?v1.2.39 "HEX Chart")

Naturally, each character pair represents 256 values (16x16 = 256). Additionally, the *#* symbol often precedes the value. Example time with the same eight colors we've been using.

![HEX](../assets/img/visuals/hex-sample.png?v1.2.39 "HEX Sample")

Feel free to continue to use color pickers, swatches, generated palettes, or any other tooling you use for color when designing. Moving forward you will simply be armed with a deeper understanding of how the color is coded. Or more precisely *encoded*.
