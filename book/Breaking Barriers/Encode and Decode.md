## Encode and Decode

Code is a system for converting meaning between forms. One form is considered *encoded* and the other *decoded*. To *encode*, is to convert *into* coded form. To *decode* is to convert *from* coded form. A few forms have been encoded and decoded already.

![Encoded and Decoded Examples](../assets/img/visuals/encoded-and-decoded-examples.png?v1.2.39 "Encoded and Decoded Examples")

A decoded form can be another code’s encoded form. The opposite is also possible. Remember this. Different environments, systems, and software sometimes only work with a specific form. The internet, browsers, and design software for example, in addition to the operating systems they run on, all need to encode and decode to do the cool things they do. Having the capability to convert forms enables better communication with each other. Each software system may expand its own capabilities by having this conversion power too. Conversion through encoding and decoding is power.

Imagine a code where a bit’s two states encoded for two RGB colors. The bit’s 1 would represent white. The bit’s 0 would represent black.

![Custom Encoding and Decoding of Black and White](../assets/img/visuals/custom-encoding-and-decoding-of-black-and-white.png?v1.2.39 "Custom Encoding and Decoding of Black and White")

What if we had to send this coded data over the internet from one side of the world to the other? Would you choose the encoded or decoded form?

Math is not my strong suit, but a single bit is twenty-four times smaller than twenty-four bits. In principle, the single bit will be delivered twenty-four times faster. Winning. This illustrates that the single bit is better for sending across the internet. However, once the bit arrives, it will not be understood as white or black until decoding occurs.

What if I told you that the decoding process took one hour? This is just a thought experiment, but if that was the case, then I would take my chances sending the twenty-four bits instead. 

The takeaway here is that encoded and decoded forms are valuable in different scenarios. As illustrated above, the desire for fast internet often means encoding data before sending and then decoding upon arrival.
