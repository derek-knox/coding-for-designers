## Bits and Bytes

A code with only two states is kind of boring. Useful yes, but boring. I bet you are thinking we can make a more interesting code if we have more than one binary. You are correct. Coders never say *more than one binary* though. It sounds weird. It also takes too long to say and type. Coders prefer shortcuts. Welcome to the word *bit*.

A *bit* is a *binary digit*. We already know binary represents one of two states. We also know that those two states are agreed to mean *1* and *0*. So a bit is a binary digit. A bit can either be a *1* or a *0*. Simple.

![One Bit](../assets/img/visuals/one-bit.png?v1.2.39 "One Bit")

One bit is boring because it can only represent one of two states. The natural question is then, how many states can we represent with *two* bits?

![Two Bits](../assets/img/visuals/two-bits.png?v1.2.39 "Two Bits")

That makes sense. One bit represents one of two possible states. Two bits represents one of four possible states. No brainer.

Those *00 or 01 or 10 or 11* sequences look weird though right? They are. Thankfully, when we get to writing our own code, we don’t have to write in *1s* and *0s*. As a designer, I would not have learned to code otherwise.

Now is a good point in time to reflect on *why* we are even looking at bits if we don’t have to code with them. There are four ideas worth instilling:

1. Computers, these complex machines, rely solely on extremely basic concepts
2. There is no magic in coding, just simple ideas stacked atop each other
3. The *one-of-two-states concept* a single bit represents is constantly reused in coding
4. The longer the *sequence of bits*, the greater the amount of *states*

How many states could we represent with three bits?

![Three Bits](../assets/img/visuals/three-bits.png?v1.2.39 "Three Bits")

Eight? I was expecting six. It makes sense though when we see each state's bit sequence. For every bit we add, we double the total possible number of states as before.

![Bits and States](../assets/img/visuals/bits-and-states.png?v1.2.39 "Bits and States")

You will notice that I stop at eight bits in the example above. We could keep going and the same doubling rule would apply. Why stop at eight then? Random I know. Ultimately, *people simply agreed that eight is a good stopping point*. They *agreed* that being able to represent 256 states was *good enough* for a wide range of codes. Again, no inherent magic.

We know coders like shortcuts. Is there a shorter way to say and type *eight bits*? Yes, welcome to the word *byte*. A *byte* is *eight bits*. Naturally, a byte also represents one of 256 states.

Let’s use what we just learned to make an example code of our own. We will use a byte’s 256 states to represent the symbols of the English language. Twenty-six lowercase and twenty-six uppercase letters would use up fifty-two states. Ten numerals and all the punctuation marks, including a lot of obscure marks, could be represented in another fifty states. We could increase fifty to one-hundred and include even more obscure marks (poop emoji included). One hundred four unused states would *still remain* (256 - 26 - 26 - 100 = 104). Good enough, for English at least.

What if we wanted a code to represent *all the symbols of every single human language we have ever known*? A single byte would not cut it. Different amounts of bits, and thus bytes, are useful for different scenarios. Chew on that.

I want to reinforce the notion that the *converted meaning* represented by a certain number of states could be *anything we want*. This is why coding is such an expressive and creative craft. This foundation of binary, bit, and byte is simply about representing *states*. As the amount of states a computer controls increases, so too does the potential power of *the code we author*.

I mentioned previously that we don’t have to write code in 1s and 0s. Thank god. Coders have done that for us already. Then other coders created their own code on top of that. This stacking of code on top of code is what will allow us to write JavaScript later. We won’t care about any of the code underneath JavaScript. We will focus only on the *high-level language* of JavaScript. We will ignore the *low-level languages* beneath it. We will just know, and respect the fact, that they are there.
