## Work. Right. Better.

In programming there is the axiom "make it right before you make it faster". This exact quote is found in [*The Elements of Programming Style* by Brian W. Kernighan and P.J. Plauger](https://www.amazon.com/Elements-Programming-Style-2nd/dp/0070342075) and it is heavily respected in the programming community. Put another way, when authoring code you should make it:
1. Work
2. Right
3. Better

In this section we will explore the notion of *right* and *better* in reference to the working `toggleImageOpacity` function that we recently looked at. This effort will introduce reusable code improvement techniques and the thought process behind them. The three step process will become second nature in time.

First and foremost you need the code to *work*. No shit. Making it *right* is less obvious though. Typically this goal is to achieve the same functionality with less or more efficient code. Making it *better* is much more subjective.

Is the code better for beginner and junior coders? Is it better for advanced and senior coders? Is it better for the computer? Sometimes the solution is ideal for all, but this is not always the case. For example an advantageous language feature may be common sense to an advanced coder, but confusing to a beginner coder. The code is better for one group and less so for the other. Similarly, when program performance is a bottleneck, better may mean sacrificing human readability. This should be avoided, but it is sometimes necessary. This latter example translates to an advantage during compilation time or execution time, but a disadvantage to authoring time.

Below is the `toggleImageOpacity` function from the previous section with added comments. Each comment is numbered for reference as we will soon be investigating the code line-by-line. After we understand each line of code, we will entertain improvement ideas.

```javascript
// 1. function declaration using `toggleImageOpacity` identifier
function toggleImageOpacity() {

    // 2. variable declaration and assignment using `imageToToggle` identifier
    var imageToToggle = document.getElementById('image-to-toggle');

    // 3. function call with 'halve-opacity' argument using `toggle` identifier 
    imageToToggle.classList.toggle('halve-opacity');

// 4. end of `toggleImageOpacity` function
}
```

Now that the numbered comments provide a bit more context to each associated line of code, let's take a deeper look into each snippet. I do not expect you to understand everything we cover in the rest of this section however. Do not be discouraged if some words or ideas make no sense yet as this is expected. The intention of this section is to plant seeds in your mind regarding the vocabulary, concepts, and considerations of advanced coders. Extract what you can before we formally enter the [*80/20 JavaScript*](/80-20-javascript) chapter.

In snippet one there are four parts comprising the line of code:
```javascript
// 1. function declaration using `toggleImageOpacity` identifier
function toggleImageOpacity() {
```
1. `function` - keyword reserved by JavaScript denoting a function
2. `toggleImageOpacity` - custom identifier for referencing the function by name
3. `()` - *function signature input* defining how to call the function to do work
4. `{` - opening curly brace for declaring the beginning of the *function body*
    - `}` - a closing curly brace is expected after the *function body* for declaring its end
    - the `{` and `}` define the scope boundary of a function

The above four parts make up the anatomy of a function. First, the `function` keyword tells the JavaScript engine your intent to define a function. Second, a custom identifier is set to enable other code to properly reference and call the function by name. Third, the function's signature input defines what argument parameters the function expects to use when doing its work. No arguments are expected for `toggleImageOpacity` currently. Lastly, the `{` and `}` define the bounds of the function body. All functions return a value prior to closing the function body too, but this *function signature output* will be covered in the next chapter. 

With respect to making snippet one *right* there is nothing we can do. A case could be made for making it *better* by renaming `toggleImageOpacity` to a single character alternative. This change is better for computers because there is less information to read which also translates to a smaller payload to send over a network. We won't make this change as we want to keep the code better for us humans. Research the automated *minification* process to learn about attaining the best of both worlds.

In snippet two there are seven parts comprising the line of code:
```javascript
// 2. variable declaration and assignment using imageToToggle identifier
var imageToToggle = document.getElementById('image-to-toggle');
```

1. `var` - keyword reserved by JavaScript denoting a variable
1. `imageToToggle` - custom identifier for referencing a value by name
1. `=` - *assignment operator* that assigns the value on its right to the identifier on its left
1. `document.getElementById` - browser API function for accessing a specific HTML element's corresponding JavaScript object
1. `()` - special characters for function execution
1. `'image-to-toggle'` - argument value used in the function's work
1. `;` - character reserved by JavaScript denoting the explicit end of a code statement

The above seven parts make up the anatomy of a declaration and assignment statement. The parts work together to assign the resulting value from the `document.getElementById('image-to-toggle')` browser API call to the declared `imageToToggle` identifier. In subsequent code, the identifier can be used as a shortcut to reference the actual element object and then use its API. It is worth noting that every use of `imageToToggle` could be replaced with `document.getElementById('image-to-toggle')`. This approach would *work*, but it would be *less right* because we'd be doing the same work more times than needed.

Assigning an executed function's result to a variable identifier is an example of *caching*. Caching is a great approach for decreasing code volume and increasing execution time performance. The more expensive and time consuming the function call, the more valuable caching is. Preventing repetitive work is the win with caching.

With respect to making snippet two *more right*, there is one other thing we could do. We could cache the `imageToToggle` lookup and assignment *outside* of the `toggleImageOpacity` function. In doing so, we would use caching and prevent the repetitive work that occurs each time `toggleImageOpacity` executes. This approach would require the identifier to never be reassigned to continue to work.

Additionally, an even *more right* approach could be made if `imageToToggle`'s value would be useful to other code within our program. If this was the case then the variable wouldn't exist in the scope just outside the `toggleImageOpacity`'s scope, but could instead exist in a higher level scope via the singleton design pattern. We mentioned this approach in the [*Programming and Visual Design - Principles and Patterns*](/programming-and-visual-design#principles-and-patterns) section, but the details are outside the scope of this book. Again, we're just planting seeds here so do not concern yourself with the details. We'll leave the snippet as is for simplicity and because there isn't anything *better* we can do.

In snippet three there are four parts comprising the line of code:
```javascript
  // 3. function call with 'halve-opacity' argument using `toggle` identifier 
  imageToToggle.classList.toggle('halve-opacity');
```

1. `imageToToggle.classList.toggle` - browser API function for toggling the existence of a class on the element
1. `()` - special characters for function execution
1. `'halve-opacity'` - argument value used in the function's work
1. `;` - character reserved by JavaScript denoting the explicit end of a code statement

The above four parts make up the anatomy of one of the many expression statements. Specifically it is an execution statement. This occurs when a function has the `()` but is not preceded by the `function` keyword. We are not declaring it, but instead executing or *using it*. The parts work together to add the `halve-opacity` class to the `imageToToggle` element if it doesn't have it already. They also work together to remove the `halve-opacity` class from the `imageToToggle` element if it does. Toggling is another situation where the binary one-of-two-states concept again surfaces.

With respect to making snippet three *right* and *better*, there is not much we can do. We could take a caching approach as mentioned earlier with something like `var toggle = imageToToggle.classList.toggle;` and then subsequently call the function with `toggle()`. This particular example is not a good use case and it is an over-optimization. Again, caching is useful for time consuming and expensive work. You will learn in time when and when not to cache.

In snippet four there is one part comprising the line of code:
```javascript
// 4. end of `toggleImageOpacity` function
}
```
1. `}` - closing curly brace for declaring the end of the *function body*
    - `{` - an opening curly brace is expected before the *function body* for declaring its beginning
    - the `{` and `}` define the scope boundary of a function
    
This closing curly brace simply defines the end of the `toggleImageOpacity` function. There are no *right* or *better* improvement possibilities.

In this section we introduced some of the vocabulary, concepts, and considerations of advanced coders. Overwhelming I know. Hopefully a few things made sense though. Regardless, seeds should be planted that will bear fruit later in your learn-to-code journey.

In the next chapter [*80/20 JavaScript*](/80-20-javascript) we will bypass much of this advanced material in favor of a simplified approach. As you progress as a coder you can begin to make code *right* and *better*. Until then, the goal is to make code that works.
