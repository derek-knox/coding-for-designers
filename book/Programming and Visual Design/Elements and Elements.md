## Elements and Elements

The elements of visual design are the *primitive* pieces of visual design. Though some may argue the exact list, we will use the one below. A related set of primitives are listed for programming code though there is not a formal one-to-one mapping. A bunch of new terms are introduced here, but don't expect to grasp them all at once. We'll continually revisit each throughout the rest of this book.

![The Elements](../assets/img/visuals/the-elements.png?v1.2.39 "The Elements")

### Point and Expression

The *point* is the first element. A point by itself may, at most, represent a single idea or value at a given time. Similarly an *expression* represents, at most, a single idea or value at a given time. Both are the smallest meaningful and composable elements of visual design and programming respectively.

Point:

![Point](../assets/img/visuals/point.png?v1.2.39 "Point")

Expression:

```javascript
'#2D7DD2'
```

### Line and Statement

More than one point or expression enables another level of meaning to be possible. For example connecting two points forms the second visual element, a *line*. Similarly, connecting expressions forms the second programming element, a *statement*. Each are the building blocks of visual design and programming respectively.

Line:

![Line](../assets/img/visuals/line.png?v1.2.39 "Line")

Statement:

```javascript
var color = '#2D7DD2';
```

### Shape and Function

Combining lines together in specific ways enables the third visual element, a *shape*. A shape is ultimately a set of lines, straight or organic, that creates an enclosure. A *function*, the third programming element, is a great way to create an enclosure of statements. Take note that a function, like a shape, is considered a self-contained unit.

Shape:

![Shape](../assets/img/visuals/shape.png?v1.2.39 "Shape")

Function:

```javascript
function changeTheColor() {
  // Function code goes here
}
```

### Space and Scope

Naturally, by creating an enclosure, you simultaneously create *space*, the fourth visual design element. In programming, this fourth element can also be thought of as space, but the term used is *scope*. Shapes allow their enclosed space to be distinct from the space outside. A function’s scope allows it to be distinct from the scope outside too. Distinct space and distinct scope allow various areas to exist simultaneously without clashing. This is super valuable when programming.

Space:

![Space](../assets/img/visuals/space.png?v1.2.39 "Space")

Scope:

```javascript
// External scope
function changeTheColor() {
  // Internal scope
}
```
### Color and Signature

*Color*, the fifth element of visual design provides a common way to differentiate similar or otherwise identical shapes. A function’s *signature* allows functions to differentiate themselves too. Adding a signature to a function, like color to a shape, gives it an identity.

Color:

![Color](../assets/img/visuals/color.png?v1.2.39 "Color")

Signature:

```javascript
function changeTheColor(newColor) {
  color = newColor;
}
```

### Value and Arguments

*Value* may be applied to a shape’s color to modify the result. Similarly in a function’s signature, different *arguments* may be applied to change the function’s result. Value allows a shape with a specific color to be reused with a different effect. Arguments allow a function with a specific signature to be reused with a different effect too.

Value:

![Value](../assets/img/visuals/value.png?v1.2.39 "Value")

Arguments:

```javascript
changeTheColor('#8D6D00');
```

### Form and Object

Grouping the aforementioned visual design elements in a particular way gives rise to a particular *form*. Similarly, grouping the aforementioned programming elements can give rise to a particular *object*. As a shape and a function are considered self-contained units, a form and an object are more powerful versions. Each embody some configuration of their respective prior elements. A form often embodies more than one shape just as an object often embodies more than one function.

Form:

![Form](../assets/img/visuals/form.png?v1.2.39 "Form")

Object:

```javascript
var someObject = {
  changeTheColor: function (newColor) {
    color = newColor;
  },
  changeSomethingElse: function () {
    // Function code goes here
  }
}
```

### Texture and State

*Texture* when applied to a shape or a form gives it a richer quality. This richness is exemplified as a sense of time, where a texture is aged and weathered for example. Similarly, a function or object with *state* gives it a richer quality, a sense of time as well. A shape or form that lacks texture often lacks richness. The same is true for a function or object that lacks state. It is worth noting that state is vital to programming where texture in a visual design is not. State is a necessity in programming interactive designs.

Texture:

![Texture](../assets/img/visuals/texture.png?v1.2.39 "Texture")

State:

```javascript
function Colorer () {
  var color = '#FFFFFF';
  var oldColor = '#000000';
  
  function changeNew (newColor) {
    oldColor = color;
    color = newColor;
  }

  function changeOld () {
    color = oldColor;
  }

  function getColor () {
    return color;
  }

  return {
    changeTheColor: changeNew,
    revertTheColor: changeOld,
    getTheColor: getColor,
  }
}
```

### Summary

The linear walk through above is intentional as each primitive builds upon the previous. Points connect to create lines just as expressions connect to create statements. By combining these lines in specific ways, a particular shape is created. The shape encloses space. Similarly, combining statements and enclosing them in a function creates a scope. Adding color to a shape gives it identity just as adding a signature to a function does. Assigning value to a color gives it a particular effect or result. Arguments assigned to a function provide a particular result as well. A form exists as a particular combination of the prior visual elements. An object, like a form, exists as a particular combination, but of the previous programming elements. Finally, richness is achieved through texture for visuals and state for code.

An infinite amount of visual and programming designs are possible using the elements above. Your creativity and experience are the only limiting factors.

Over time, the visual and programming communities at large acknowledged common patterns that were useful in using the aforementioned elements. In visual design these are known as the *design principles*. In programming they are called *design patterns*. Both are extremely useful in their respective domains.
