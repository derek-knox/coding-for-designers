## Behavior

Behavior, as previously mentioned, is where the structural building blocks in combination with their adornments _react_. They react to interaction, the environment, and ultimately time. Behavior enables one or more building blocks and one or more adornments to change in time. These changes manifest in an infinite amount of ways.

The possible behaviors, and their respective ease of application, is contextual. In the context of 2D with the web platform, behavior is applied via JavaScript code. In the context of 3D and Unity, behavior is applied via C# code. Each coding language, in conjunction with each platform’s authoring tools, provide ways to help you apply certain behavior. For example, Unity with C# provides simple ways (compared to the web and JavaScript) to apply behavior in 3D space.

Behaviors may be different depending on the context, but their fundamental purpose remains constant. Behavior brings life to a design. It does so in the form of interactivity, motion, and logic. For example, a design with behavior may manifest as a creative authoring environment, an entertaining animation, or a mission critical tool. The manifestations are endless and limited only by your imagination.

Let’s explore some specific applications of behavior by building off the previous structure and style examples.

We know behavior may be applied to structure (a specific building block or group of building blocks). Additionally, we know behavior may be applied to style (adornments). The sheer presence of behavior however, due to its dynamic nature, results in an additional target we have yet to talk about. Time itself. Time in the form of delays and schedules are most common.

Now is a good time to explicitly identify example triggers that result in behavioral reactions. A trigger is _input_ and the reaction produces _output_. Without input triggers a design remains static and lacks dynamism. It lacks life. There are three types of input triggers:

1. User interaction (tap, click, hover, gesture, voice, etc.)
2. Environment (layout resizing, operating system, device sensors, etc.)
3. Time (delays, schedules, etc.)

As you can imagine, the output possibilities are endless as a result of any of the above input triggers. This is where you get to be super creative.

New building blocks or groups of building blocks may be added to the structure dynamically. They can also be removed dynamically. Adornments may be added or removed dynamically too. Behavioral changes to these structures or adornments are just as easily modifiable.

![Input Trigger Output Examples](../assets/img/visuals/input-trigger-output-example.png?v1.2.39 'Input Trigger Output Examples')

By reusing a small HTML snippet from above, we can see corresponding examples of the graphics in the image above:

Left:

```markup
<body>
    <h1></h1>
    <h2></h2>
</body>
```

Middle:

```markup
<body>
    <h1></h1>
    <h2>
        <span class="yellow-mode"></span>
    </h2>
</body>
```

Right:

```markup
<body>
    <h2>
        <span class="red-mode"></span>
    </h2>
</body>
```

The takeaway is that there are three input trigger types that provide an opportunity for output behavior. The specific inputs and outputs are up to you, the creator.

Where traditional visual design is static, behavior makes a design dynamic and interactive. This power is addictive, useful, and extremely empowering. In the context of the web platform and Unity, instead of using solely what the JavaScript and C# code provides, you can _extend_ each language's capabilities with your _own_ code. You can invent entirely new and better ways to do almost anything. Super cool.

Next we will look at the pieces that makeup the ability to _code behavior_. We will do so in the familiar context of the Elements, Principles, and Constructs of Visual Design. Rock on!
