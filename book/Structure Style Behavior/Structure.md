## Structure

Structure, as previously mentioned, is a particular arrangement of a set of building blocks. In simpler terms, structure is organization. Structural designs are the organization of structural building blocks.

These building blocks are contextual. In the context of 2D with the web platform, these building blocks are HTML elements (HyperText Markup Language). In the context of 3D with Unity, these building blocks are GameObjects.

The building blocks are different depending on the context, but the underlying relationships between them are identical. Reusable knowledge is the win here. These relationships are referred to as *parent-child* relationships, a *tree*. In both a 2D and a 3D structure, a tree of parent-child relationships exists. The same parent-child relationships—the same tree structure—can have many layout variations in both 2D and 3D. Structure is not layout, it is simply the tree of parent-child relationships.

The *body* for an HTML document and the *scene* for Unity are each the root visual parent for their respective platforms. The simplest tree structure, in both 2D and 3D, consists of only this root parent. This is a one-node tree. 

![One-Node Tree](../assets/img/visuals/one-node-tree.png?v1.2.39 "One-Node Tree")

Pretty boring eh? Here is what that looks like for HTML:

```markup
<body></body>
```

Yup, still boring. The second simplest structure is a two-node tree. This is when the root node has a single child node.

![Two-Node Tree](../assets/img/visuals/two-node-tree.png?v1.2.39 "Two-Node Tree")

Here are two HTML examples:

```markup
<body>
    <h1></h1>
</body>
```

```markup
<body>
    <div></div>
</body>
```

Still boring, but less so. The third simplest structure has two variants:
1. Two child nodes
2. A single child that is also a parent due to itself containing one child

Here are HTML examples of each respectively:

```markup
<body>
    <h1></h1>
    <h2></h2>
</body>
```

```markup
<body>
    <div>
        <h1></h1>
    </div>
</body>
```

Now it is getting interesting. These are both examples of three-node trees.

![Three-Node Trees](../assets/img/visuals/three-node-trees.png?v1.2.39 "Three-Node Trees")

The structural variations building off these examples, as you can imagine, are infinite.

Generally speaking, each node is just that... a node. Specifically however each is also a *specific type of node*. The general case if often referred to as the *abstract* version where the specific case is a *concrete* version. The `<body>`, `<h1>`, `<h2>`, and `<div>` are all concrete versions of a generic representation. This abstract vs. concrete aspect will make sense later as you learn more about programming so don't worry about it for now. The images above are intended to demonstrate the general case, but you can envision the child nodes as being color coded by their concrete type if that helps.

It may be surprising, but the only difference between 2D and 3D structural trees are the building block types associated with the nodes. The variations of the parent-child relationships are identical for 2D and 3D. What results is still a tree structure. This is the takeaway.

The computer program—the web browser for our 2D examples and the Unity Engine for our 3D examples—knows how to decode a tree structure. Each program then outputs a 2D or 3D rendering respectively. It is important to understand that the tree structure simply defines the relationships between the building blocks. HTML elements for 2D with the web platform and GameObjects for 3D with the Unity platform.

As our custom defined structure is respected and properly reflected by each platform, we may begin to stylize each building block. This stylization process ensures particular aesthetic or functional effects are present for the viewer. As visual designers, this is usually the most fun and rewarding part. Style time.
