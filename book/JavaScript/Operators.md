## Operators

Expressions are synonymous with values. Values by themselves are useful, but they are more so when they can be *operated* on. Put another way, being able to assign, combine, access, and create new values is useful.

JavaScript provides certain reserved keywords and special characters that fall into this operator category. There are over fifty but with our subset approach we only care about sixteen. You know most of them already from elementary math class. Bonus. Additionally, there is an extremely useful character that's not technically an operator but might as well be. We'll start there. Welcome to the *dot* (`.`).

### Dot Notation

The dot is not an operator, but a notation. This is just fancy talk. We'll consider it an operator.

The dot allows us to access an object's nested keywords and thus its nested values. You have already seen this in action through virtually every code snippet up to this point. Refresher time:

```javascript
function makeBackgroundBlack() {
    document.body.style.backgroundColor = '#000000';
}

makeBackgroundBlack();
```

There are three uses of the dot in the familiar snippet above. In each subsequent use a specific keyword of a nested object is accessed. Accessing a specific keyword of an object—and thus its value—is exactly what the dot is for.

If you recall from the [*Interactive Code*](/interactive-code) chapter, a JavaScript program at execution time is just a tree of function executions and thus a tree of scopes. Since a function is also a certain type of `Object` (the `Function` Object) a JavaScript program is also a *tree of objects*.

A JavaScript program is a tree of:
- Objects
- Functions
- Scopes

The dot character is powerful because it allows us to *navigate objects*. By navigating objects, we can search for and access specific and nested values to work with. The moment a dot provides access to a nested keyword value is the moment operators become useful.

### Assignment Operator

The most commonly used operator is the *assignment* operator. It is the equal (`=`) sign. You already know what it does, but you are likely used to seeing it work with numbers only. In JavaScript, you assign keyword identifiers specific values with it. These values can be primitive (like `360`, `'360'`, and `true`) or complex (like `{}` and `[]`). They can be literal expressions (like `360`) or evaluated expressions too (like `300 + 60`). In all cases, the engine works to provide a single value that is then assigned to a specific keyword identifier. Simple. Here we go again:

```javascript
function makeBackgroundBlack() {
    document.body.style.backgroundColor = '#000000';
}

makeBackgroundBlack();
```

In the above snippet, the engine's work is simple. After `makeBackgroundBlack()` is called, the value of focus is the literal hex color String `'#000000'`. It is assigned to the `backgroundColor` keyword of the `style` object of the `body` object of the `document` object. As an aside, we now know the `backgroundColor` keyword is on an object nested three levels deep in the program tree.

Assignment is useful for one fundamental reason. What do you think it is?

Without assignment, we would never be able to *change or save values*. This would mean new values could be created during execution time, but no other code would be able to use them. That would make for lame games, tools, and software. Thank you `=`. 

### Arithmetic Operators

We won't spend much time on the arithmetic operators as you already know about them. They are:

- `+` Addition
- `-` Subtraction
- `*` Multiplication
- `/` Division

These four operators in combination with the assignment operator give us coders a ton of power. For example, we gain the core ability to animate and otherwise move, resize, scale, and transform visual elements to our desire. In fact, here is a primitive example using HTML (structure), CSS (style), and JavaScript (behavior):

HTML
```markup
<div id='the-brick' class='brick'>I'm a brick</div>
```

CSS
```css
.brick {
    background-color: #FF0000;
    display: inline-block;
}
```

JavaScript
```javascript
var theBrick = document.getElementById('the-brick');
var angle = 0;

function updateBrickRotation() {
    angle = angle + 1;
    theBrick.style.transform = 'rotate(' + angle + 'deg)';
}
  
setInterval(updateBrickRotation, 16);
```

Take a moment to envision what the combination of snippets results in before reading on. Spoiler alert, it's a spinning brick. This example illustrates how we can use the arithmetic and assignment operators together to make changes over time.

Remember the *Interactive Code - Frame Rate* section? Animation is simply one or more changes that impact visual elements between each rendered frame. With code we can make and apply these changes to visual elements over time. It's as simple as that.

### String Concatenation Operator

The line of code in the example above that is *applying* the rotation change is:

```javascript
theBrick.style.transform = 'rotate(' + angle + 'deg)';
```

We've seen this String Concatenation Operator (`+`) before in our `artboards.js` file:

```javascript
alert('No artboard to delete. None of the ' + artboards.length + ' artboards are in focus.');
```

Yes, the `+` is *both* an arithmetic operator *and* the String Concatenation Operator. When used only with numbers, it is the arithmetic operator and when Strings are involved, it is the String Concatenation Operator.

If we pretend that the `angle` value is `45` and `artboards.length` is `3`, then our snippets become:

```javascript
theBrick.style.transform = 'rotate(' + '45' + 'deg)';
```

and 

```javascript
alert('No artboard to delete. None of the ' + '3' + ' artboards are in focus.');
```

After the concatenation operator does its work (adding Strings together), the end result of each snippet is:

```javascript
theBrick.style.transform = 'rotate(45deg)';
```

and

```javascript
alert('No artboard to delete. None of the 3 artboards are in focus.');
```

When expressions are evaluated and a resulting value remains (specific Strings in our two examples above), this is the moment in time they are useful. What if we only wanted to make changes over time *given a certain condition*? This is where the comparison operators come in.

### Comparison Operators

These comparison operators are vital for controlling *code flow*. Put another way, only certain code executes at a given moment in time based on one or more conditions. We'll update the brick JavaScript snippet from above to extend our animation example:

```javascript
var theBrick = document.getElementById('the-brick');
var isClockwise = true;
var angle = 0;

function toggleIsClockwise() {
    isClockwise = !isClockwise;
}

function updateBrickRotation() {
    if (isClockwise === true) {
        angle = angle + 1;
    } else if (isClockwise === false) {
        angle = angle - 1;
    }

    theBrick.style.transform = 'rotate(' + angle + 'deg)';
}
  
setInterval(updateBrickRotation, 16);
setInterval(toggleIsClockwise, 2000);
```

We added four new pieces:
1. `isClockwise` identifier
1. `toggleIsClockwise()` function
1. `if` statement
1. `setInterval(toggleIsClockwise, 2000)` function call

How do you think the above additions change the program? There is a new operator (`!`) added that we'll soon cover, but still try to guess how the program changes.

Here is added context for each addition:
1. `isClockwise` identifier (to track clockwise rotation over time)
1. `toggleIsClockwise()` function (to alternate `isClockwise`)
1. `if` statement (to control code flow)
1. `setInterval(toggleIsClockwise, 2000)` function call (to execute `toggleIsClockwise` every 2000 milliseconds)

You nailed it if you guessed that the changes make the brick's rotation toggle between a clockwise and counter-clockwise rotation every two seconds.

The main comparison operator in use is the `===`, but there are six in total:
- `===` Strict equality
- `!==` Strict inequality
- `>` Greater than
- `>=` Greater than or equal to
- `<` Less than
- `<=` Less than or equal to

It is worth noting that the `===` and `!==` are useful with Strings just as they are with Numbers. In fact the strict equality and strict inequality operators are useful for comparing *all* types of values. In all cases, the expression—the *resulting value of the comparison*—is a Boolean value. If the Boolean is `true` the code flows that way. If `false`, the code flow skips that way.

With these six operator options you can validate certain conditions and thus control code flow. You'll notice that in the example above and in all cases we care about, the comparison operators are used with variations of `if` statements. We've yet to explicitly cover these statements in hopes that you can intuit what they do on your own. We will cover them in the following [*Statements*](#statements) section however to solidify your understanding.

Of the four additions in the previous code example, the one that should be the most odd has to do with the `toggleIsClockwise` function and specifically its body:

```javascript
isClockwise = !isClockwise;
```

This should be the case due to the uncovered `!` operator. It is one of three logical operators.

### Logical Operators

The logical operators are useful for controlling code flow just like the comparison operators. As such, they too operate on Boolean values. They are:

- `!` NOT
- `&&` AND
- `||` OR

Think of the NOT operator as a shortcut to a function that *flips* the Boolean value it is attached to. Here is the example shortcut function:

```javascript
function flipTheBoolean(booleanArgument) {
    if (booleanArgument === true) {
        return false;
    } else if (booleanArgument === false) {
        return true;
    }
}
```

Put another way, by flipping a Boolean we produce its opposite. A `true` becomes `false` and a `false` becomes `true`. Pretty simple. The `!` is just odd looking. If we refresh back to our `toggleIsClockwise` function body:

```javascript
isClockwise = !isClockwise;
```

We can instead envision:

```javascript
isClockwise = flipTheBoolean(isClockwise);
```

So naturally, when `isClockwise` is `true` its flipped value is `false` and vice versa. In our example, this new flipped value is then assigned via the assignment operator to the `isClockwise` keyword identifier. It may seem odd to use the `isClockwise` keyword identifier twice on the same line and you are right. It becomes less odd in time and especially after we dig deeper in the *Statements* section.

The AND (`&&`) and OR (`||`) operators are useful for working with *more than one* Boolean at a time. An example helps illustrate this:

```javascript
var red = '#FF0000';
var green = '#00FF00';
var blue = '#0000FF';
var currentColor = red;

function randomizeBackgroundColor() {
    var randomNumber = Math.random();
    var isRed = randomNumber >= 0 && randomNumber < .33;
    var isGreen = randomNumber >= .33 && randomNumber < .66;
    var isBlue = randomNumber >= .66 && randomNumber <= 1;

    if (isRed) {
        currentColor = red;
    } else if (isGreen) {
        currentColor = green;
    } else if (isBlue) {
        currentColor = blue;
    }

    document.body.style.backgroundColor = currentColor;
}

setInterval(randomizeBackgroundColor, 1000);
```

What happens in the above snippet during execution time?

You nailed it if you said every second the `document`'s `backgroundColor` gets randomly updated to either red, green, or blue. We use the `&&` operator to work with two Boolean values in each `isRed`, `isGreen`, and `isBlue` assignment. Then `currentColor` is assigned a color based on the code flow. For example, the color blue is assigned to `currentColor` only if `isBlue` is `true`. It only becomes `true` if the `randomNumber` is greater than or equal to `.66` *and* `randomNumber` is less than or equal to `1`. In that scenario both `isRed` and `isGreen` will be `false` where the code flowed past each conditional check. That's code flow in action using the `&&` operator.

Let's look at code flow in action using the `||` operator:

```javascript
var volume = 0;

function updateVolume(keyEvent) {
    var isArrowUpPressed = keyEvent.key === 'ArrowUp';
    var isArrowRightPressed = keyEvent.key === 'ArrowRight';
    var isIncrease = isArrowUpPressed || isArrowRightPressed;

    if (isIncrease) {    
        volume = volume + 1;
    }
}

document.addEventListener('keydown', updateVolume);
```

What happens in the above snippet during execution time?

You nailed it if you said that the `volume` increases when the up or right arrow keys are pressed. Nothing happens if any other keys are pressed as our code flow check is only using `isIncrease` (which was assigned with the help of `||`).

This program rocks a little too hard because the volume can't be turned down. Think how you might fix that before reading on.

Answer time:

```javascript
var volume = 0;

function updateVolume(keyEvent) {
    var isArrowUpPressed = keyEvent.key === 'ArrowUp';
    var isArrowDownPressed = keyEvent.key === 'ArrowDown';
    var isArrowLeftPressed = keyEvent.key === 'ArrowLeft';
    var isArrowRightPressed = keyEvent.key === 'ArrowRight';
    var isIncrease = isArrowUpPressed || isArrowRightPressed;
    var isDecrease = isArrowDownPressed || isArrowLeftPressed;

    if (isIncrease) {    
        volume = volume + 1;
    } else if (isDecrease) {
        volume = volume - 1;
    }
}

document.addEventListener('keydown', updateVolume);
```

Each of the `&&` and `||` examples above only illustrate their use with two Booleans. This is intentional. As mentioned earlier however, they are useful with *more than one* Boolean. Two is not the limit. It is recommended however to keep multiple uses at a minimum to reduce complexity. This is especially true for us regarding our 80/20 approach.

### New Operator

If you recall the from the *Keywords - Reserved Keywords* section, the `new` keyword was introduced. It was and still is a *helper for creating unique instances*. An *instance* is simply a unique version of a specific Object. The specific Object can be built-in or custom:

- `var date = new Date();` (built-in)
- `var artboard = new Artboard();` (custom)

By having unique instances we can dynamically create *new* objects for use in our program. Let's build on the `artboard.js` example that we introduced in the *Expressions* section. Here it is again for reference:

```javascript
var createArtboardButton = document.getElementById('create');
var deleteArtboardButton = document.getElementById('delete');
var artboards = [];
var artboardInFocus;

function setupEventListeners() {
    createArtboardButton.addEventListener('click', onCreateArtboardButtonClick);
    deleteArtboardButton.addEventListener('click', onDeleteArtboardButtonClick);
}

function updateArtboardInFocus(artboard) {
    artboardInFocus = artboard;
}

function deleteArtboardInFocus() {
    var artboardInFocusIndex = artboards.indexOf(artboardInFocus);
    artboards.splice(artboardInFocusIndex, 1);
    artboardInFocus.removeSelfFromSurface();
    artboardInFocus = null;
}

function onCreateArtboardButtonClick() {
    var artboard = new Artboard();
    artboards.push(artboard);
    artboard.addSelfToSurface();
    updateArtboardInFocus(artboard);
}

function onDeleteArtboardButtonClick() {
    if (artboardInFocus === null) {
        alert('No artboard to delete. None of the ' + artboards.length + ' artboards are in focus.');
    } else if (artboardInFocus === undefined) {
        alert('No artboard to delete. Try creating one first.');
    } else {
        deleteArtboardInFocus();
    }
}

setupEventListeners();
```

You will notice that there is no `Artboard` function. If it was a built-in type then there would be no problem. Since it is a custom type we do. An error will result when `onCreateArtboardButtonClick()` executes due to a `click` on the `createArtboardButton`. We need to declare and define this `Artboard` function so our program will work. If we do not, then the code evaluates to `new undefined();`. This is an error as `undefined` is not a function and thus cannot be called.

We will add our `Artboard` function just under the `var`iable declarations. Again, in time you'll intuit where and how to best group your functions to organize your code.

```javascript
function Artboard() {
    var artboardElement;
    var api = {
            addSelfToSurface: add,
            removeSelfFromSurface: remove
        };

    function initialize() {
        artboardElement = document.createElement('div');
        artboardElement.classList.add('artboard');
    }

    function add() {
        document.body.appendChild(artboardElement);
    }

    function remove() {
        document.body.removeChild(artboardElement);
    }

    initialize();

    return api;
}
```

Now new artboards can be created by simply calling:

```javascript
var artboard = new Artboard();
```

This is exactly what the `new` operator is useful for. Since we are explicitly `return`ing an Object literal, we could instead:

```javascript
var artboard = Artboard();
```

This would be the more right and better approach. For our purposes however feel free to use the `new` operator and think of it as your helper for getting unique instances of built-in, third-party, and custom types of Objects.

We have covered quite a few code snippets up to this point and every single one of them is comprised of at least one *statement*. We'll cover statements in detail now as you have gone long enough using your intuition without validation.