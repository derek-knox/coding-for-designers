## Statements

If you recall from the [*Programming and Visual Design - Elements and Elements*](/programming-and-visual-design#elements-and-elements) section, we learned that a statement in programming is similar to the line in visual design:

> For example connecting two points forms the second visual element, a *line*. Similarly, connecting expressions forms the second programming element, a *statement*. Each are the building blocks of visual design and programming respectively.

Additionally, in the [*Mindset*](#mindset) section earlier we explored *Thinking in Three Zoom Levels* and specifically *Zoom Level 2 - Statement Pattern*. No statement patterns have been explicitly covered yet in favor of letting your intuition guide you. Now is the time to cover them however. There are over twenty-five, but we only care about six:

Primary
- Variable Declaration
- Function Declaration
- Expression

Secondary
- If
- Return
- Debugger

We use the term *statement pattern* as a statement is simply a particular pattern of characters and keywords. You will recognize each of the above six as they've been used in previous code snippets. Now you will learn each statement pattern by name for improved understanding and communication.

The order of each statement above is intentional as this is the general order you should expect to see them in code. Consider the primary statements to always be present in a program where the secondary ones are less guaranteed.

Anytime you see one or more `var`iable declarations, you can consider them as the start of some meaningful group of code. Following these declarations, you should see one or more `function` declarations. The `var`iables exist to denote that these keywords—and ultimately their values—will be useful to the declared `function`s when executed. A program typically starts by at least one `function` executing via `()`. This is one example of an expression statement.

These `function` declarations that follow will almost always use the prior `var`iable declarations. We say almost always as the program is interactive. If a user doesn't take certain actions or a certain trigger doesn't occur, some functions simply will not execute. We author them so they are prepared to however.

The rest of the statements help further control code flow, return values of interest from `function`s, and help with debugging.

If it hasn't been obvious in the snippets up to this point, three general things happen in order:

1. Declare `var`iables for reuse in `function`s
1. Declare `function`s to do work (often using the `var`iables)
1. Execute one or more `function`s with `()`

By mapping each statement type to the above three steps we have:

1. Variable Declaration
1. Function Declaration
1. Expression, If, Return, and Debugger

When thinking about programming in these terms, programming is much simpler to reason about. Deeply understanding these six statements and their general three step order gives you a lot of power. As we dig deeper into each, your exposure to and knowledge of keywords, expressions, and operators will come full circle.

### Variable Declaration Statement

The goal of the Variable Declaration Statement is to *declare a `var`iable*. This statement is useful because it allows us to identify a value container by name for later reuse. Again, if we couldn't save values, our programs wouldn't be able to do much. Here is an example of a `var`iable declaration:

```javascript
var maximumRotation;
```

The anatomy of this type of statement consists of three parts in sequence:

1. `var` keyword
1. custom keyword naming the `var`iable
1. `;` character

You are familiar with the first two, but the `;` has not been covered. What do you think it means? In comparing the above statement to an English sentence, what do you think the `;` would be equivalent to?

If you said the period, you are correct. The `;` simply denotes that the coder intentionally ends the statement. So in our example above we are telling the engine:

"Hey engine, please create a `var`iable named `maximumRotation`."

The statement pattern is valid (no `Error`s result) so the engine does what we say. Nice. In most snippets we've covered thus far we make the engine do a little more work than just a `var`iable declaration:

```javascript
var maximumRotation = 360;
```

You guessed it:

"Hey engine, please create a `var`iable named `maximumRotation` and while you are at it, assign it the value `360`."

The engine sees this as a valid statement too and does what we say. There is a small caveat that we need to understand however. If you recall from the [*Interactive Code - Authoring, Compiling, and Executing*](interactive-code/#authoring-compiling-and-executing) section we learned that the compile step *transforms our static authored code into code that can be executed*. As a result of compilation, this last snippet actually becomes two:

```javascript
var maximumRotation = undefined;

maximumRotation = 360;
```

When we talked about `undefined` earlier being the default, now you see how.

As we'll see in the Function Declaration Statement a similar thing happens. This aspect is one of the fundamental reasons we looked at authoring, compiling, and executing. Internalize the distinction of the two snippets due to the compiling step and you will be ahead of most in understanding JavaScript.

So our first snippet is our authored code. The second is the result of our compiled code. Now during execution, the *values* actually get assigned and operated on as the engine executes line by line and statement by statement.

### Function Declaration Statement

The goal of the Function Declaration Statement is to *declare a `function`*. This statement is useful because it allows us to identify a `function` by name for later reuse. Again, `function`s allow us to encode work, often for instantaneous results. Here is an example of a `function` declaration:

```javascript
function getMaximumRotation() {
    return maximumRotation;
}
```

The anatomy of this type of statement consists of four parts in sequence:

1. `function` keyword
1. custom keyword naming the `function`
1. `()` characters enclosing the optional *argument parameters*
1. `{}` characters enclosing the *`function` body*

So in our example above we are telling the engine:

"Hey engine, please create a `function` named `getMaximumRotation`. Set its value to the remainder of the statement, but you can ignore it until executed."

Below is another familiar example. It has a different name, it uses one argument parameter, and its body differs due it doing different work:

```javascript
function changeBackgroundColor(newColor) {
    document.body.style.backgroundColor = newColor;
}
```

In this example we are telling the engine:

"Hey engine, please create a `function` named `changeBackgroundColor`. Set its value to the remainder of the statement, but you can ignore it until executed."

The engine only looks at the argument parameters and `function` body when the `function` is executed. Etch this in your brain. Only after a function is called via `()`—not when it is declared—does the engine do the work inside the body.

At this execution moment, a similar caveat as mentioned in the [Variable Declaration Statement](#variable-declaration-statement) section is at play. Only the `changeBackgroundColor` declaration is impacted as only it has argument parameters. As a result of compilation, the `changeBackgroundColor('#FFFFFF')` executes as:

```javascript
function changeBackgroundColor(newColor) {
    var newColor = undefined;

    newColor = '#FFFFFF';
    document.body.style.backgroundColor = newColor;
}
```

This `funtion` only expects one argument whose `var`iable name is predefined. Since `var`iables are `undefined` by default, that process still occurs. This would occur for each argument parameter. Only after that, if an argument is actually passed in does its value get assigned. If we instead called `changeBackgroundColor()` without an argument value, the result would be:

```javascript
function changeBackgroundColor(newColor) {
    var newColor = undefined;

    document.body.style.backgroundColor = newColor;
}
```

The background color of the document would not update as `undefined` isn't a valid color. One approach to making this function more robust would be to add a `defaultColor` to fallback to:

```javascript
function changeBackgroundColor(newColor) {
    var defaultColor = '#000000';

    if (newColor === undefined) {
        document.body.style.backgroundColor = defaultColor;
    } else {
        document.body.style.backgroundColor = newColor;
    }
}
```

Based off what we learned about the Variable Declaration Statement and Function Declaration Statement, the compiled result for a `changeBackgroundColor()` call is now:

```javascript
function changeBackgroundColor(newColor) {
    var defaultColor = undefined;
    var newColor = undefined;

    defaultColor = '#000000';

    if (newColor === undefined) {
        document.body.style.backgroundColor = defaultColor;
    } else {
        document.body.style.backgroundColor = newColor;
    }
}
```

Where a `changeBackgroundColor('#FFFFFF')` call is:

```javascript
function changeBackgroundColor(newColor) {
    var defaultColor = undefined;
    var newColor = undefined;

    defaultColor = '#000000';
    newColor = '#FFFFFF';

    if (newColor === undefined) {
        document.body.style.backgroundColor = defaultColor;
    } else {
        document.body.style.backgroundColor = newColor;
    }
}
```

The takeaway is that `var`iables are `undefined` by default where `function`s are predefined by the language, environment, or by a coder. In either case their declaration (due to compiling) exists at the top of the scope before any assignment or `function` executions occur. Each time a `function` is executed, you can think of it as being `new`ed. We will explore this in more detail in the [*Functions*](#functions) section that soon follows.

### Expression Statement

The goal of an Expression Statement is to *do work*. This statement is useful because it allows us to assign, combine, access, and create values. Here are a few examples of expression statements:

- `var maximumRotation = 360;`
- `return maximumRotation;`
- `changeBackgroundColor('#FFFFFF');`
- `newColor = '#FFFFFF';`
- `document.body.style.backgroundColor = newColor;`

There is no specific anatomy for this type of statement. We know expressions are synonymous with values, so consider an Expression Statement one that:

- uses values
- that isn't solely a declaration (`var`iable or `function`) 

Put another way, Expression Statements are how work actually happens. A `function` execution occurs (which is itself an Expression Statement) to kick off that `function`'s work. The body of said `function` uses one or more Expression Statements—in combination with `var`iable declarations and other `function` declarations—to do work.

All three of the primary statements in concert make programs useful:

- Variable Declaration Statements for resuing values by name
- Function Declaration Statements for reusing patterns of work by name
- Expression Statements for using values (literal or evaluated)

### If Statement

The goal of an If Statement is to *control code flow*. This statement is useful because it allows or prevents code from running conditionally. These conditions are validated with Booleans. Here is an example of an `if` statement:

```javascript
if (isClockwise) {
    alert('isClockwise is true');
}
```

The anatomy of this type of statement consists of three parts in sequence:

1. `if` keyword
1. `()` characters enclosing one or more Boolean expressions
1. `{}` characters enclosing the conditional code to execute

There are two additional variations that leverage the `else` keyword and the `else if` keywords together. You can chain as many of these `else if` statements to the main `if` where the `else` is always last or non-existent. Here is an `if...else` example.

```javascript
if (isClockwise) {
    alert('isClockwise is true');
} else {
    alert('isClockwise is false');
}
```

And here is an `if...else if` example:

```javascript
if (isRed) {
    currentColor = red;
} else if (isGreen) {
    currentColor = green;
} else if (isBlue) {
    currentColor = blue;
}
```

Building a little further, here is an `if...else if...else` example:

```javascript
if (isRed) {
    currentColor = red;
} else if (isGreen) {
    currentColor = green;
} else if (isBlue) {
    currentColor = blue;
} else {
    currentColor = defaultColor;
}
```

The takeaway is that variations of the `if`, `else if`, and `else` keywords are vital to controlling code flow.

### Return Statement

The goal of a Return Statement is to *return a specific value from a `function`*. This statement is useful because it returns a value based on the `function`'s work. Here is an example of a `return` statement:

```javascript
return maximumRotation;
```

The anatomy of this type of statement consists of three parts in sequence:

1. `return` keyword
1. an expression
1. `;` character

A `return` means that the expression value will be sent out of the `function`. No code after the `;` will run on this `function`'s execution. This approach is very useful for assigning a `var`iable the result of a `function`'s work. Here are two examples:


```javascript
var date = new Date();
```

and

```javascript
var artboard = new Artboard();
```

The takeaway is that the `return` statement is useful for providing a value that can be saved for later use.

### Debugger Statement

The goal of a Debugger Statement is to *stop the executing program at an exact moment in time*. This statement is useful during debugging because it freezes code execution and automatically opens the browser's developer tools. Each browser's developer tools help a coder understand what certain values are during execution. We'll dive a bit deeper later in the *Debugging* section. Here is an example of a `debugger` statement:

```javascript
debugger;
```

The anatomy of this type of statement consists of two parts in sequence:

1. `debugger` keyword
1. `;` character

The takeaway is that the `debugger` statement is useful for stopping execution at an exact moment in time to investigate values. This statement is only for coders. Remove it when your program is live for end-users.