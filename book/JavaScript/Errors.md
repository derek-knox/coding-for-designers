## Errors

Errors suck. They are inevitable however. Even the most seasoned professionals encounter them. A professional's knowledge and experience simply increases his or her chance of a quick resolution.

To help you more quickly resolve errors, we'll focus on the subset that you are most likely to encounter. There are three specific types of `Error`s worth focusing on:

- `SyntaxError`
- `ReferenceError`
- `TypeError`

`SyntaxError`s relate to typos and they are experienced during authoring or compile time. The other two types—`ReferenceError` and `TypeError`—fundamentally revolve around `undefined` or `null` and they are experienced during execution time. Let's dig a little deeper into each so you have a concrete understanding of what is happening when you encounter them. 

### `SyntaxError`

When you encounter a `SyntaxError`, a specific code statement will likely have:

- invalid identifier(s) or character(s)
- missing required character(s)

In either case, a typo exists. The code will accidentally contain one or more unknown characters in the first case. As for the second case, an expected character will be missing.

Below are a few examples, each with an associated comment. The comment denotes the specific error details that would display in a browser's developer tools console during execution time. We'll explore these tools in the [*Debugging*](#debugging) section later in this chapter.

```javascript
// Uncaught SyntaxError: Unexpected identifier
vars color = '#FFFFFF';
```

What is wrong with the above code snippet? The `vars` should be `var`. The compiler program doesn't know what `vars` is and throws a `SyntaxError` because it doesn't know what to do. We could have a snippet like `var vars = 'vars';` because `vars` in this example would be a valid `var`iable identifier name and `'vars'` a valid string value. Typos matter.

```javascript
// Uncaught SyntaxError: Invalid or unexpected token
var color = #FFFFFF;
```

What is wrong with the above code snippet? If you said our color definition isn't a valid string value then you nailed it. Our `#FFFFFF` must become `'#FFFFFF'` or `"#FFFFFF"` to become valid. Again, typos matter.

Do yourself a favor and mentally link a `SyntaxError` to a typo. From here you will have at least identified the core problem so you may more quickly resolve the error. Debugging tools will further help you.

### `ReferenceError`

When you encounter a `ReferenceError`, a specific code statement will likely have:

- referenced an undeclared identifier
- referenced an identifier outside the current scope chain

In either case, an identifier is inaccessible. The scope chain will be traversed in the first case, but the identifier will not be found—because it was never declared. As for the second case, the identifier may have been declared but its declaration exists outside the current scope chain.

Below are a few examples, each with an associated comment as before.

```javascript
// Uncaught ReferenceError: selectedColor is not defined
var colorInFocus = selectedColor;
```

What is wrong with the above code snippet? The syntax is correct, so no issue there. However, `selectedColor` does not exist in the scope chain. It is an undeclared identifier. Declarations matter.

```javascript
function rollDice() {
  var sideCount = 6;
  var randomDiceValue = Math.ceil(Math.random() * sideCount);
  return randomDiceValue;
}

// Uncaught ReferenceError: sideCount is not defined
console.log('The ' + sideCount + '-sided dice roll result is ' + rollDice());
```

What is wrong with the above code snippet? You nailed it if you recognized that `sideCount` is only accessible in the `rollDice()` `function`'s scope. The `sideCount` identifier declaration exists, but it is not in the scope chain of the `console.log` statement referencing it. Declarations—and the scope in which they are declared—matter.

Refresh with the [*Functions - Scope Chain*](#scope-chain) section if your understanding of how the scope chain works is still a little fuzzy. 

### `TypeError`

When you encounter a `TypeError`, a specific code statement will likely have:

- attempted to access a nested identifier of an `undefined` or `null` object
- called an identifier with `()` as if it referenced a `function`

In either case, a basic or specific `Object` type is misused. The code will reference an identifier of an `undefined` or `null` value in the first case. Each of these special values are literal values that don't contain nested identifiers. As for the second case, an identifier will be used as a `function` when it is not one.

As before, below are a few examples, each with an associated comment.

```javascript
var blackObject = { name: 'black', color: '#000000' };
var whiteObject;

// Uncaught TypeError: Cannot read property 'name' of undefined
console.log("Oreo cookies are " + blackObject.name + " and " + whiteObject.name);
```

What is wrong with the above code snippet? As you may recall, our `whiteObject` is `undefined` by default. We forgot to assign it a value. Accessing a nested identifier—`name` in this example—of the `undefined` value is an error. The same is true if our `whiteObject`'s assigned value was `null`. Using types properly matters.

```javascript
var blackObject = { name: 'black', color: '#000000' };
var whiteObject = { name: 'white', color: '#FFFFFF' };

// Uncaught TypeError: blackObject.name is not a function
console.log("Oreo cookies are " + blackObject.name() + " and " + whiteObject.name;
```

What is wrong with the above code snippet? We updated our `whiteObject` by assigning it a value to resolve our issue in the previous snippet. However, we are now accidentally calling `blackObject.name` as if it is a `function`. It is a string, not a `function` though so we get an error as a result. Using types properly matters.

### Debugging

Each comment associated with the error examples above was provided for context. It doesn't show up in the code otherwise. To see `Errors`, you need to use debugging tools.

Most browsers have a built-in subprogram called something like *developer tools*. Unsurprisingly these tools are for us coders. With them, any website's underlying HTML, CSS, and JavaScript code is explorable. This is a big deal and extremely useful. These tools help us understand how a particular website or web app works. It is advantageous to use the tools while quickly iterating during the author, compile, and execution time cycle. They provide another view into our code that isn't just the rendered output of our creation.

Of the many built-in developer tools, there are three that will be most useful to you. There exact names vary among browsers, but they'll closely align to the following:
1. Elements - view the HTML structure in real-time
1. Console - view `console.log` output and `Error`s in real-time
1. Sources - view the source code of files

All three are super useful, but the console is where you'll see a `SyntaxError`, `ReferenceError`, or `TypeError`. A clean console is the goal. It is best practice to remove your `console.log` statements after you've confirmed they produce the expected output.

The `debugger` statement is another useful debugging tool that was previously mentioned. It is useful because it opens the developer tools at the exact line of code it exists on. Additionally, execution freezes—until it is resumed—so state can be examined at an exact moment in time. Super powerful.

Lastly, your editor is another useful tool that is immensely empowering. It improves authoring time, but it can be additionally useful for debugging. Some common features of editors include:
- code-completion - help prevent typos and maintain code consistency
- color coding - aid the scanning and reading of code
- linter - help prevent typos and maintain code consistency
- snippets - help prevent typos and maintain code consistency

All of them additionally speed up authoring time.

Though code can be authored using a basic text editor, it's worth using a sophisticated text editor and other visual code generation tools. They give you super powers in comparison.

Errors still suck, but you are better equipped to resolve them now.