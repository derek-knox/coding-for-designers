## Functions

A function is like a shape because it encloses scope just as a shape encloses space. This enclosure helps prevent clashing of identifiers between different scopes. At execution time, a JavaScript program is a nested tree of function executions. Since each executing function encloses a scope, a JavaScript program is a nested tree of scopes. This tree changes because the stack of function executions grows and shrinks over time.

Functions are paramount to JavaScript programming for this organizational reason. They additionally serve two other fundamental use cases totaling three.

Function as:

- Organizational unit
- Instantiable unit
- Reusable work unit

Often times a function serves two or all three at once. Let's dive into each to take our understanding to the next level. First however, let's quickly refresh on the anatomy of a function as it is the same in all three use cases.

### Anatomy of a Function

The anatomy of a function, as we learned in the *Function Declaration Statement* section, consists of four parts in sequence. We have also learned that the *Return Statement* is extremely useful in functions. What we have not yet covered is the fact that a `return` *always exists*.

There is actually a fifth part of the anatomy of a function, but it wasn't important to reveal until now. This fifth part is an automatically inserted `return undefined;` if a custom `return` isn't the last statement of the function body. This automatic insertion occurs just as `var = undefined;` does due to compilation. Here is the updated anatomy:

1. `function` keyword
1. custom keyword naming the `function`
1. `()` characters enclosing the optional *argument parameters*
1. `{}` characters enclosing the *function body*
1. `return undefined;` at end of *function body* if a custom `return` isn't defined there

Due to this anatomy, a function always has `return undefined;` as its last statement unless a custom `return` is defined instead. For example our familiar:

```javascript
function makeBackgroundBlack() {
    document.body.style.backgroundColor = '#000000';
}
```

after compilation becomes:

```javascript
function makeBackgroundBlack() {
    document.body.style.backgroundColor = '#000000';
    return undefined;
}
```

In most cases, this does not matter as the same work gets done. What is nice about knowing the anatomy is that *all functions work like this*. Built-in ones, third-party ones, and custom ones. Internalizing the anatomy of a function empowers you to understand any function you encounter in addition to its `return` value. This fact in combination with the *thinking in three zoom levels* technique will help you read, understand, and author code. The name, argument parameters, and the work itself of functions will differ, but the pattern remains. Etch this pattern in your brain.

In [*Interactive Code - Anatomy of HTML, CSS, and JavaScript - JavaScript*](/interactive-code/#javascript) we learned that a function is structured in two parts:

1. Reference work
2. Core work

Reference work manifests as Variable Declaration and Function Declaration statements. The core work manifests as everything else. Our `makeBackgroundBlack` snippet doesn't need any reference work help so it just does core work. Functions are your friend.

### Organizational Unit

All functions by default are an organizational unit as they enclose a scope (just as a shape encloses space). By separating and nesting functions coders can *intentionally* organize a program to their liking. This scope organization also allows identifiers (`var`iable or `function`) of the same name to exist in different scopes without clashing. These identifiers must be declared in *different functions* for this to work. These are the core takeaways. Example time:

```javascript
function PortraitArtboardElement() {
    var artboardElement = document.createElement('div');
    artboardElement.classList.add('portrait-artboard');
    return artboardElement;
}

function LandscapeArtboardElement() {
    var artboardElement = document.createElement('div');
    artboardElement.classList.add('landscape-artboard');
    return artboardElement;
}
```

In the example above we *organize* our code by declaring the `PortraitArtboardElement` and `LandscapeArtboardElement` functions. You will notice that the `var artboardElement` is declared twice, once in each function. This is not a problem however as each function encloses its own scope. Based on the example snippet below, how many `artboardElement` `var`iables do you think would exist in the program?

```javascript
var portraitArtboardElement1 = new PortraitArtboardElement();
var portraitArtboardElement2 = new PortraitArtboardElement();
var landscapeArtboardElement1 = new LandscapeArtboardElement();
var landscapeArtboardElement2 = new LandscapeArtboardElement();
```

If you guessed four, you are correct. Each `var artboardElement` declaration is *unique to each function execution*. This transitions us to the *instantiable unit* use case.

### Instantiable Unit

The instantiable unit use case is very common and useful when a function's purpose is to provide a *unique instance of a specific Object*. Built-in, third-party, and custom specific Objects can apply. We have seen a few examples of this:

- `var date = new Date();` (built-in)
- `var artboard = new Artboard();` (custom)
- `var portraitArtboardElement1 = new PortraitArtboardElement();` (custom)

In each example a unique instance is `return`ed and assigned to the respective `var`iable. From this point on, each can be further used in the program where the dot operator provides deeper access into the instance. This dot access manifests in one of two ways:

1. reading an identifier value
1. writing (updating) an identifier value using `=`

Without unique instances our programs would be extremely limited as you can imagine.

Take note of the intentional use of the `UpperCamelCase` naming convention. This is used to denote that a function `return`s a specific Object instance. You might now be wondering, what is the `lowerCamelCase` naming convention useful for? This transitions us to the *reusable work unit*.

### Reusable Work Unit

The reusable work unit use case is also very common. It is useful for repetitive work. Additionally, the `lowerCamelCase` naming convention is used to distinguish it from the `UpperCamelCase` instantiable unit use case. We have seen numerous examples of this:

- `makeBackgroundBlack();`
- `changeBackgroundColor('#FFFFFF');`
- `toggleImageOpacity();`
- `getMaximumRotation();`
- `toggleIsClockwise();`
- `updateBrickRotation();`
- `updateArtboardInFocus(artboard);`
- `onCreateArtboardButtonClick();`

This type of function embodies the *Mindset - Don't Repeat Yourself* technique. By authoring functions to be flexible and reusable, more can be accomplished with less. Intuitively authoring functions this way takes time but it is a goal to strive for. Ultimately, these types of functions are useful anytime a trigger occurs where work should be done in response. All the function examples above fall in this category.

You may recall the brief mention of *iterating an `Array`* in the *Expressions* section. To iterate an `Array` is to *do work using each item*. This is a perfect use case for reusable work. In fact, iteration is so common that the `Array` type has built-in helper functions. There are five specifically that are extremely useful and worth learning. Here they are in alphabetical order:

- `filter()` - conditionally `return` each item
- `find()` - find the first item that meets a condition and `return` it
- `forEach()` - do work using each item
- `map()` - transform each item into a new value and `return` the new value
- `reduce()` - reduce all items to a single value and `return` that value

These five functions will be useful for most—if not all—of the code you author involving `Array`s. Let's look at examples of each to see how they are useful in practice. Take note that the `colors` and `widths` `Array` identifiers below are plural not singular. This is a best practice.

#### `filter()`

The `filter()` function is useful as it enables us to conditionally `return` each item. Additionally, this function creates a new `Array` automatically for us that contains only these filtered items. In the example below we iterate through each item—colors in this case—and `return` a Boolean value indicating our intention to keep (`true`) or not keep (`false`) the item. This new list of filtered colors is `return`ed from the `filter()` call. We go one step further and assign it to `colorsExcludingBlackAndWhite`.

```javascript
var colors = ['#FFFFFF', '#FF0000', '#00FF00', '#0000FF', '#000000'];
var colorsExcludingBlackAndWhite;

function colorFilter(item) {
    if (item === '#FFFFFF' || item === '#000000') {
        return false;
    } else {
        return true;
    }
}

colorsExcludingBlackAndWhite = colors.filter(colorFilter);
```

Since a function is a value, *we can pass it as an argument to another function*. Mind blown.

#### `find()`

The `find()` function is useful as it enables us to find the first item that meets a condition and then `return` it. In the example below we iterate through each item—colors in this case—*until* the condition is `true` for a given item. When this occurs, that item is `return`ed from the `find()` call. We go one step further and assign it to `greenColor`.

```javascript
var colors = ['#FFFFFF', '#FF0000', '#00FF00', '#0000FF', '#000000'];
var greenColor;

function findGreenColor(item) {
    return item === '#00FF00';
}

greenColor = colors.find(findGreenColor);
```

You guessed it, since a function is a value, we can pass it as an argument to another function.

#### `forEach()`

The `forEach()` function is useful as it enables us to do work using each item. In the example below we iterate through each item—colors in this case—and use it to set the `backgroundColor` of a newly created `<div>` element.

```javascript
var colors = ['#FFFFFF', '#FF0000', '#00FF00', '#0000FF', '#000000'];

function createColorSwatch(item) {
    var swatchElement = document.createElement('div');
    swatchElement.classList.add('color-swatch');
    swatchElement.style.backgroundColor = item;
    document.body.appendChild(swatchElement);
}

colors.forEach(createColorSwatch);
```

In the case of `forEach()` we pass it the function identifier `createColorSwatch`. Then as part of `forEach()`'s work it calls `createColorSwatch` for each item in our `colors` array. The value—a color in this case—of each iteration is passed as the argument to `createColorSwatch`. Imagine if `colors` consisted of one-hundred colors instead of five. Iteration is powerful and extremely useful.

#### `map()`

The `map()` function is useful as it enables us to transform each item into a new value and `return` the new value. Additionally, this function creates a new `Array` automatically for us that contains these new values. In the example below we iterate through each item—colors in this case—and use it to create a transparent variation. This new list of transparent colors is `return`ed from the `map()` call. We go one step further and assign it to `transparentColors`. Though we didn't below, we could go a step further still with a `transparentColors.forEach(createColorSwatch);` call. Reuse in action.

```javascript
var colors = ['#FFFFFF', '#FF0000', '#00FF00', '#0000FF', '#000000'];
var transparentColors;

function halveOpacity(item) {
    var transparentColor = item + '80';
    return transparentColor;
}

transparentColors = colors.map(halveOpacity);
```

Again, since a function is a value, we can pass it as an argument to another function. It will take time to get used to this powerful idea.

#### `reduce()`

The `reduce()` function is useful as it enables us reduce all items to a single value and then `return` that value. In the example below we iterate through each item—numbers in this case—and `return` the current reduced value. This current reduced value is reused again as `previousItem` in the next iteration. This repeats until all values have been iterated over. Put another way, we iterate through each `item` and combine it with `previousItem` into a single value. This reduced value is `return`ed from the `reduce()` call. We go one step further and assign it to `totalWidth`.

```javascript
var widths = [100, 300, 100];
var totalWidth;

function widthReducer(previousItem, item) {
    return previousItem + item;
}

totalWidth = widths.reduce(widthReducer);
```

The `Number` `500` is the value of `totalWidth` now. One more time. A function is a value and we can pass it as an argument to another function.

It will take time for these five `Array` functions to become second nature. Look them up to remind yourself how they work and to understand what argument parameters are expected. Remember that you are not alone as professionals—like beginners—need to reference resources. The takeaway is that if you need to iterate, look to these five functions for help.

### Scope

We know that in order for a function to do its work, it must be called using `()` with any expected arguments in-between. The first example we saw of this was the last line of:

```javascript
function makeBackgroundBlack() {
    document.body.style.backgroundColor = '#000000';
}

makeBackgroundBlack();
```

During execution time two questions surface regarding the snippet:

1. How is `document` accessed if not declared in `makeBackgroundBlack`'s scope?
1. How did the snippet start executing in the first place?

To answer these questions we need to understand the *scope chain* and *execution context*. Understanding these will bring full circle our final *Zoom Level 3 - Value Resolution* technique.

#### Scope Chain

A literal value is obviously one of the primitive or complex types. This is easy to resolve. Resolving an *identifier's* value is not obvious. As such, the scope chain is useful for *resolving identifier values*. We briefly covered how it works in the [*Interactive Code - Anatomy of HTML, CSS, and JavaScript - JavaScript*](/interactive-code/#javascript) section. Now we give it a name and dive a little deeper.

When the JavaScript engine evaluates an expression that has an identifier reference (shortcut name for a value), it works like this in an effort to get the bound value:

1. Look in this function's scope for the identifier
2. If not found, look in that scope's parent scope
3. Repeat until the identifier is found or the root parent scope is hit

This process defines the scope chain. With it, the found identifier's value is used in place of the identifier itself. The identifier value is now resolved. Winning. We know a JavaScript program is a nested tree of scopes. The scope chain refers to the process of walking up this scope tree in search of an identifier and thus its value.

So to answer question one above the process is:

1. look for `document` in the current scope (`makeBackgroundBlack` scope)
    - not found
1. look for `document` in the current scope's parent (global scope)
    - `window` is the browser environment's global object
    - `window.document` is found
1. replace the initial statement's `window.document` with the found value
    - the value is a a specific `Object` defined by the environment
1. continue work using this found value

As we saw in the *Keywords - Non-Reserved Keywords - Environment* section, the value assigned to `window.document`—by the environment—is a specific `Object`. To refresh, here is the partial example:

```javascript
window.document = {
    body: {
        style: {
            backgroundColor: ''
        }
    }
}
```

Here is another example to showcase the same scope chain process using a custom identifier and a nesting of `function`s:

```javascript
var someIdentifier = 'Winning!';

function one() {

    function two() {

        function three() {
            console.log(someIdentifier);
        }

        three();
    }

    two();
}

one();
```

The identifier `someIdentifier` does not exist in `three`'s scope (nor does `console` for that matter). It does not exist in `two`'s or `one`'s either (same with `console`). It does however exist in the global scope (`console` does too). As a result, `console.log('Winning!')` executes as `someIdentifer` resolves to `Winning!` where `console` resolves to an environment object.

The takeaway is that a child scope can look to a parent scope for an identifier. It does not work the other way around. Additionally, the nesting of scopes has no limit of depth. Thank you scope chain.

What happens if an identifier is *not* found? In these scenarios we have an error.

```javascript
function doSomeWork() {
    console.log(someUndeclaredIdentifier);
    console.log('This never executes :(');
}

doSomeWork();
```

Our program breaks when the above snippet is executed. Additionally, no further statements after the error will be executed. Our program is broken. This is not good.

It is important for us to use our editor, `debugger`, and a browser's developer tools to prevent errors. With our developer tools open we would see `Uncaught ReferenceError: someUndeclaredIdentifier is not defined`. We will explain this and other common `Error`s in the *Errors* section.

#### Execution Context

To answer the *how did the snippet start executing in the first place?* question we need to more deeply understand *execution context*. We have and will continue to just use the term scope however.

We know a function encloses a scope just as a shape encloses space. A scope only ever exists when a function gets *executed* though. So the code:

```javascript
function changeBackgroundColor(newColor) {
    document.body.style.backgroundColor = newColor;
}
```

Is only a *single* Function Declaration Statement with *no scope* where each execution of it results in a *new scope*. Our understanding of [*Interactive Code - Authoring, Compiling, and Executing*](/interactive-code#authoring-compiling-and-executing) is of use here.

At authoring time we declare and define this `changeBackgroundColor` `function` *knowing* that each call of it during execution time will result in a new scope. Each execution—each new scope—manifests as an addition to the stack. The stack *grows*. When the top most `function` on the stack `return`s, this is the moment our stack *shrinks*. The event loop becomes unblocked once all the `function` executions have `return`ed.

For each execution the `newColor` identifier's value may be different. It differs based on the argument value passed in. This fact ensures the snippet below results in four distinct scopes where each `changeBackgroundColor` call has a different `newValue` as a result. The same `document` value is used due to the scope chain. Pretty sweet.

```javascript
changeBackgroundColor('#FF0000');
changeBackgroundColor('#00FF00');
changeBackgroundColor('#0000FF');
```

There are four and not three scopes in play above. What is the fourth? If you guessed the global scope then you are correct. This leads us to finally answering *how did the snippet start executing in the first place?* Do you remember the `<script>` tag introduced in the [*Interactive Code - Anatomy of HTML, CSS, and JavaScript - JavaScript*](/interactive-code/#javascript) section? Bingo.

Each `<script>` is essentially a function call. The stack grows. Once the last line of the `<script>`'s code is executed, it `return`s. The stack shrinks.

It's been implied that all snippets thus far exist in a particular `.js` file loaded by the `<script>` tag's `src` attr. Our above snippet can be thought of as:

```javascript
function global1() {
    changeBackgroundColor('#FF0000');
    changeBackgroundColor('#00FF00');
    changeBackgroundColor('#0000FF');
}

global1();
```

We can load many different `<script>` tags allowing us to further organize our code in unique `.js` files. Declaring and calling `global1` is done by the engine. The stack grows and shrinks just as if we defined and called `global1` ourselves.

The takeaway is that the scope chain is used to resolve identifier values. Understanding it is a requirement for the computer to run our program. When us coders understand it, we know how to substitute an identifier with the proper value. When we understand execution context, we see how scopes exist and how code gets executed in the first place. Thank you `<script>` tag.