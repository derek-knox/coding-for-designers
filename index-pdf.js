var Metalsmith = require('metalsmith');
var pdfize = require('metalsmith-pdfize');

/*
Though a pain in the ass, I was only able to get the pdf export working by explicitly overriding pdfize.js like so:

```javascript
function serveMetalsmithFiles(files) {
    return function (req, res) {
        const localPath = req.url.replace(/^\/+/, '');

        // if ( !files[localPath] ) {
        //     console.warn(`[metalsmith-pdfize] Unable to serve "${req.url}" from file list`);
        //     res.writeHead(404);
        //     return res.end();
        // }
        res.writeHead(200);
        res.end(files['preface\\index.html'].contents);
    };
}
```
*/

Metalsmith(__dirname)
  .source('./site')
  .destination('./pdf')
  .use(pdfize({
    pattern: ['**/index.html'],
    printOptions: {
        printBackground: true,
        format: 'A4',
        margin: {
          "top": "1cm",
          "bottom": "1cm",
          "left": "1cm",
          "right": "1cm"
        }
    },
  }))
  .build(function (err, files) {
    if (err) { throw err; }
  });